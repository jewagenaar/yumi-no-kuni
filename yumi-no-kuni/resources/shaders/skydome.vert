#pragma debug(on)

uniform vec4 u_LightPos;

void  main() {        
    gl_TexCoord[0] = gl_MultiTexCoord0;
    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}