#pragma debug(on)

uniform vec4 u_LightPos;

/* variables related to the player light source */
varying vec4 plsPos; // the light position
varying vec3 plsV;     // eye view vertex
varying vec3 plsN;     // eye view normal
varying vec3 plsD;     // eye view light vector

/* variables related to the far light source */
varying vec4 fixPos; // the light position
varying vec3 fixV;     // eye view vertex
varying vec3 fixN;     // eye view normal
varying vec3 fixD;     // eye view light vector

void  main() {    
    plsPos = vec4(0.0, 1.0, -14.0, 1.0);
    plsV   = vec3(gl_ModelViewMatrix * gl_Vertex);
    plsN   = normalize(gl_NormalMatrix * gl_Normal);
    plsD   = normalize(plsPos.xyz - plsV);
    
    fixPos = u_LightPos * gl_ModelViewMatrix;
    fixV   = vec3(gl_ModelViewMatrix * gl_Vertex);
    fixN   = normalize(gl_NormalMatrix * gl_Normal);
    fixD   = normalize(fixPos.xyz - fixV);

    gl_TexCoord[0] = gl_MultiTexCoord0;
    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}