
/* variables for the player light source */
varying vec4 plsPos; // the position
varying vec3 plsV;   // eye view vertex
varying vec3 plsN;   // eye view normal
varying vec3 plsD;   // eye view light vector

/* variables related to the far light source */
varying vec4 fixPos; // the light position
varying vec3 fixV;   // eye view vertex
varying vec3 fixN;   // eye view normal
varying vec3 fixD;   // eye view light vector

uniform sampler2D u_TextureSampler;

float kc = 0.8; // constant attenuation
float kl = 0.1; // linear attenuation
float kq = 0.1; // quadratic attenuation

// calculate the k-factor for the point light source
float kfactor() {
    vec3 P = plsPos.xyz;
    vec3 Q = plsV.xyz;
    vec3 L = P - Q;

    float d = sqrt(dot(L, L));
    float result = 0.6 / (kc + kl*d + kq*d*d);

    return result;
}

// calculate the amount of diffuse light reaching the fragment
float diffuse() {
    float  result = 0.2 * max(dot(fixN, fixD), 0.0);
    return result;
}

void main() {
    float k = kfactor();
    float d = diffuse();

    gl_FragColor = (k+d)*texture2D(u_TextureSampler, gl_TexCoord[0].st);
}