uniform sampler2D u_BumpMapSampler;
uniform sampler2D u_TextureSampler;

vec4 plain() {
    vec4 color  = texture2D(u_TextureSampler, gl_TexCoord[0].st);
    return color;
}

void main() {
    gl_FragColor = plain();
}