varying vec3 V;
varying vec3 D;
varying vec3 N;
varying vec2 texco;
varying vec3 lightPos;

uniform sampler2D u_TextureSampler;

void main() {
	vec4 color  = texture2D(u_TextureSampler, gl_TexCoord[0].st);
    gl_FragColor = clamp(max(dot(D, N), 0.0) * 0.09, 0.0, 1.0) * color;;
}