varying vec3 V;
varying vec3 D;
varying vec3 N;
varying vec2 texco;
varying vec3 lightPos;

uniform sampler2D u_TextureSampler;

void main() {
    vec3  a = lightPos.xyz;
    vec3  b = V.xyz;
    vec3  d = b - a;
    float k = sqrt(dot(d, d));
    float f = clamp(5.0 / k, 0.1, 0.9);

	vec4 color  = texture2D(u_TextureSampler, gl_TexCoord[0].st);

    gl_FragColor = f*clamp(max(dot(D, N), 0.0) * 0.1, 0.2, 0.8) * color;
}