#pragma debug(on)

varying vec3 V;
varying vec3 D;
varying vec3 N;
varying vec2 texco;
varying vec3 lightPos;

void main()
{
    //stuff being passed to fragment shader
    lightPos = vec3(0.0, 1.0, -14.0);

    V = vec3(gl_ModelViewMatrix * gl_Vertex);
    N = gl_NormalMatrix * gl_Normal;
    D = normalize(lightPos.xyz - V);
    
    gl_Position    = gl_ModelViewProjectionMatrix * gl_Vertex;
    gl_TexCoord[0] = gl_MultiTexCoord0;
}
          