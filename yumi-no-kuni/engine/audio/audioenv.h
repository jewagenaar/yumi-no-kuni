//
//  audioplayer.h
//  yumi-no-kuni
//
//  Created by John-Evans Wagenaar on 02/05/2013.
//  Copyright (c) 2013 John-Evans Wagenaar. All rights reserved.
//

#ifndef __yumi_no_kuni__audioenv__
#define __yumi_no_kuni__audioenv__

#include "../base/kami.h"
#include "../base/primitives.h"

#include <iostream>
#include <thread>
#include <queue>
#include <map>

#include <OpenAL/al.h>
#include <OpenAL/alc.h>

namespace kami
{
    // audio environment implementation
    class kamiaudio
    {
        ALuint   *m_buffer;
        ALuint   *m_sources;
        uint16_t  m_buffercount;
        
        std::mutex  m_mutex;
        bool        m_running;
        
        // OpenAL resources
        ALCdevice  *m_device;
        ALCcontext *m_context;
        
        // queue for sounds to be played
        std::queue<ALuint> m_soundqueue;
        
    public:
        kamiaudio();
        
        // load a wav file and return the source id for it
        ALuint load(const std::string &filename, bool looping);
        
        // play all sounds in the queue
        void execute();
        
        // add sounds to the queue
        void enqueue(const ALuint &sound_id);
        
        // setup and shutdown methods
        void init();
        void stop();
    };
    
    // facade namespace for the audio environment
    namespace audioenv
    {
        void start();
        void stop();
        
        kamiaudio *env();
        
        ALenum get_openal_format(kami::wav *data);
    }
}


#endif /* defined(__yumi_no_kuni__audioenv__) */
