//
//  audioplayer.cpp
//  yumi-no-kuni
//
//  Created by John-Evans Wagenaar on 02/05/2013.
//  Copyright (c) 2013 John-Evans Wagenaar. All rights reserved.
//

#include "audioenv.h"
#include <iostream>

#include "../utils/fileutils.h"

using namespace kami;

ALfloat source_pos[] = { 0.0, 0.0, 0.0 };
ALfloat source_vel[] = { 0.0, 0.0, 0.0 };

ALfloat listener_pos[] = { 0.0, 0.0, 0.0 };
ALfloat listener_vel[] = { 0.0, 0.0, 0.0 };
ALfloat listener_dir[] = { 0.0, 0.0, -1.0,  0.0, 1.0, 0.0 };

kamiaudio   *kami_audio   = 0;
std::thread *audio_thread = 0;

/* ================ */
void audioenv::start()
{
    if (::audio_thread) {
        std::cerr << "only one audio thread can be active at a time" << std::endl;
    } else {
        ::kami_audio = new kamiaudio;
        ::kami_audio->init();
        
        // start a lambda thread to execute the 
        ::audio_thread = new std::thread([](){
            while (true) {
                ::kami_audio->execute();
            }
        });
    }
}

/* =============== */
void audioenv::stop()
{
    ::kami_audio  ->stop();
    ::audio_thread->join();
    
    delete ::kami_audio;
    ::kami_audio = 0;
    
    delete ::audio_thread;
    ::audio_thread = 0;
}

/* ==================== */
kamiaudio *audioenv::env()
{
    return ::kami_audio;
}


/* ======================================= */
ALenum audioenv::get_openal_format(wav *data)
{
    if (data->num_channels == 1 && data->bits_per_sample == 16) {
        return AL_FORMAT_MONO16;
    }
    
    if (data->num_channels == 2) {
        if (data->bits_per_sample == 16) {
            return AL_FORMAT_STEREO16;
        }
        
        if (data->bits_per_sample == 8) {
            return AL_FORMAT_STEREO8;
        }
    }
    
    // we default to 8 bit mono - meh...
    return AL_FORMAT_MONO8;
}

/* ================ */
kamiaudio::kamiaudio() :
m_buffer(new ALuint[KAMI_NUM_AUDIO_BUFFERS]),
m_sources(new ALuint[KAMI_NUM_AUDIO_BUFFERS]),
m_buffercount(0)
{
    
}

/* ============================================================ */
ALuint kamiaudio::load(const std::string & filename, bool looping)
{
    kami::wav *wav_data = fileutils::load_wav_file(filename);
    
    ALenum format      = audioenv::get_openal_format(wav_data);
    ALint  sample_rate = wav_data->sample_rate;
    ALint  size        = wav_data->data_size;
    ALbyte *data       = reinterpret_cast<ALbyte *>(wav_data->data);
    
    alBufferData(m_buffer[m_buffercount], format, data, size, sample_rate);
    
    alSourcei (m_sources[m_buffercount], AL_BUFFER,   m_buffer[m_buffercount]);
    alSourcef (m_sources[m_buffercount], AL_PITCH,    1.0f);
    alSourcef (m_sources[m_buffercount], AL_GAIN,     1.0f);
    alSourcefv(m_sources[m_buffercount], AL_POSITION, ::source_pos);
    alSourcefv(m_sources[m_buffercount], AL_VELOCITY, ::source_vel);
    alSourcei (m_sources[m_buffercount], AL_LOOPING,  looping);
    
    fileutils::dispose_wav(wav_data);
    
    ALuint result = m_sources[m_buffercount];
    m_buffercount++;
    
    return result;
}

/* =================== */
void kamiaudio::execute()
{
    while(!m_soundqueue.empty()) {
        ALuint sound_id = m_soundqueue.front();
        alSourcePlay(sound_id);
        m_soundqueue.pop();
    }
}

/* ========================================= */
void kamiaudio::enqueue(const ALuint &sound_id)
{
    m_soundqueue.push(sound_id);
}

/* ================ */
void kamiaudio::stop()
{
    m_running = false;
    
    alDeleteBuffers(KAMI_NUM_AUDIO_BUFFERS, m_buffer);
    alDeleteSources(KAMI_NUM_AUDIO_BUFFERS, m_sources);
    
    alcMakeContextCurrent(NULL);
    alcDestroyContext(m_context);
    alcCloseDevice(m_device);
    
    delete [] m_buffer;
}

/* ================ */
void kamiaudio::init()
{
    // create device and init context
    m_device = alcOpenDevice(0);
    if (m_device) {
        m_context = alcCreateContext(m_device, 0);
        alcMakeContextCurrent(m_context);
    }
    
    // generate some buffers and sources
    alGenBuffers(KAMI_NUM_AUDIO_BUFFERS, m_buffer);
    alGenSources(KAMI_NUM_AUDIO_BUFFERS, m_sources);
    
    // set up a listener
    alListenerfv(AL_POSITION,    ::listener_pos);
    alListenerfv(AL_VELOCITY,    ::listener_vel);
    alListenerfv(AL_ORIENTATION, ::listener_dir);
}




