//
//  math.h
//  kami
//
//  Created by John Evans Wagenaar on 2012/04/01.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef kami_math_h
#define kami_math_h

#include <math.h>
#include "primitives.h"

namespace kami
{
    // Vector math functions
    namespace vmath
    {
        // Calculate the dot product of a.b
        float dot(const vector &a, const vector &b);
        
        // Calculate the length of the vector v
        float len(const vector &v);
        
        // Get a the angle of rotation of vector v about z relative to the x-axis
        float calc_rot(const vector &v);
        
        // Normalize the vector v (make it's length = 1)
        vector normalize(const vector &v);
        
        // Add vectors a and b
        vector add(const vector &a, const vector &b);
        
        // Subtract vector b from a
        vector sub(const vector &a, const vector &b);
        
        // Flip the direction of vector v
        vector flip(const vector &v);
        
        // Multiply the vector v with the scalar s
        vector smult(const float &s, const vector &v);
        
        // Calculate the cross product of a and b
        vector cross(const vector &a, const vector &b);
        
        // Calculate the projection of vector a onto vector b
        vector proj(const vector &a, const vector &b);
        
        // Calculate the length of the projection of vector a onto b
        float proj_len(const vector &a, const vector &b);
        
        // Translate the point along the vector
        point translate(const point &p, const vector &v);
        
        // Translate the vertex along the vector
        inline void inline_translate(vertex *v, const vector &dir) {
            v->x = v->x + dir.x;
            v->y = v->y + dir.y;
            v->z = v->z + dir.z;
        }
        
        // Inline vector addition
        inline void inline_add(vector &a, const vector &b) {
            a.x += b.x;
            a.y += b.y;
            a.z += b.z;
        }
    }
    
    // point based math functions
    namespace pmath
    {
        //flip the point p
        point flip(const point &p);
        
        //calculate the distance between two points
        float dist(const point &a, const point &b);
        
        //translate the point along the vector
        point translate(const point &p, const vector &dir);
        
        //get a vector from two points
        vector vector_from_point(const point &a, const point &b);
        
        vector vector_from_point(const point2d &a, const point2d &b);
        
        /**
         * Get the projected point on plane.
         * point  p    : the point
         * normal n    : the plane normal
         * float delta : the distance to the plane
         */
        point point_on_plane(const point &p, const normal &n, const float &delta);
        
        //inline translate
        inline void inline_translate(point *p, const vector &dir)
        {
            p->x = p->x + dir.x;
            p->y = p->y + dir.y;
            p->z = p->z + dir.z;
        }
    }
    
    // absolute float
    inline float fabs(const float &f)
    {
        return f < 0.0f ? -f : f;
    }
    
    // calculate and set the tangent vector for a kami::polyface
    inline void calc_tangent(polyface & pface)
    {
        vector a = pmath::vector_from_point(pface.vb, pface.va);
        
        vector u = pmath::vector_from_point(pface.tb, pface.ta);
        vector v = pmath::vector_from_point(pface.tc, pface.ta);
        
        float f = u.x*v.y - v.x*u.y;
        f = 1.0/f;
        
        vector result = {
            a.x * f,
            a.y * f,
            a.z * f
        };
        
        pface.tn = result;
    }
}


#endif
