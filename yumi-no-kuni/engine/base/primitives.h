//
//  primitives.h
//  kami
//
//  Created by John Evans Wagenaar on 2012/03/17.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef kami_primitives_h
#define kami_primitives_h

#include "kami.h"
#include <stdint.h>

#define NULL_VEC {0.0f, 0.0f, 0.0f}

#define INIT_VOP(X, V)          X.x = V;   X.y = V;   X.z = V;
#define COPY_VOP(X, Y)          X.x = Y.x; X.y = Y.y; X.z = Y.z;
#define ASGN_VOP(X, v1, v2, v3) X.x = v1;  X.y = v2;  X.z = v3;

#define READ_BYTES(S, X, N) S.read(reinterpret_cast<char *>(&X), N) 

namespace kami
{
    typedef uint8_t floor_plan[FLOOR_SIZE];
    
    typedef uint8_t byte;
    
    // struct for holding bitmap data
    typedef struct t_bitmap {
        //BMP HEADER
        char     tag[2];
        uint32_t filesize;
        uint32_t offset_to_pixels;
        uint32_t header_size;
        uint32_t img_width;
        uint32_t img_height;
        uint16_t planes;
        uint16_t bpp;
        uint32_t size;
        bool     alpha;
        byte     *data;
    } bitmap;
    
    // struct for holding WAV data
    typedef struct t_wav {
        char     tag[4];
        uint32_t chunk_size;
        char     format[4];
        
        char     fmt_tag[4];
        uint32_t fmt_size;
        uint16_t audio_format;
        uint16_t num_channels;
        uint32_t sample_rate;
        uint32_t byte_rate;
        uint16_t block_align;
        uint16_t bits_per_sample;
        
        char     data_tag[4];
        uint32_t data_size;
        byte     *data;
    } wav;
    
    //3D point
    typedef struct t_point {
        float x;
        float y;
        float z;
    } point, vertex;
    
    //R3 vectors 
    typedef struct t_vector {
        float x;
        float y;
        float z;
    } vector, normal;
    
    //2D point
    typedef struct t_point2d {
        float x;
        float y;
    } point2d, texco;
    
    // rectangle (float)
    typedef struct t_rect {
        float x;
        float y;
        float w;
        float h;
    } rect;
    
    // rectangle (int)
    typedef struct t_irect {
        int x;
        int y;
        int w;
        int h;
    } irect;
    
    //RGB color
    typedef struct t_rgb_color {
        float r;
        float g; 
        float b;
        float a;
    } rgb_color;
    
    //converter for values
    typedef union t_vconverter {
        vector v;
        vertex vrt;
        normal n;
        point p;
        rgb_color c;
    } vconverter;
    
    //Rotation
    typedef struct t_rotation {
        float pitch;
        float roll;
        float yaw;
    } rotation;
    
    //4x4 matrix
    typedef float matrix_4x4[16];
    
    //face detail
    typedef struct t_polyface {
        vertex va;
        vertex vb;
        vertex vc;
        
        normal na;
        normal nb;
        normal nc;
        
        texco ta;
        texco tb;
        texco tc;
        
        vector tn; 
    } polyface;
    
    //polygon
    typedef struct t_polygon {
        unsigned short a; //index of vertex a
        unsigned short b; //index of vertex b
        unsigned short c; //index of vertex c
        unsigned short face_info; //face visibility info
    } polygon;
}
#endif
