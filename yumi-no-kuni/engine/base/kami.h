//
//  kami.h
//  kami
//
//  Created by John Evans Wagenaar on 2012/03/21.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef kami_kami_h
#define kami_kami_h

#define __KAMI_DEBUG__


// debug line indices
#define KAMI_DL_AVATAR 0
#define KAMI_DL_PHYSIC 1

// font indentifiers
#define KAMI_DEBUG_FONT 0xB00B

// reserved model id for bboxes
#define KAMI_BBOX_MODEL 0x00BB

// engine wide constants
#define KAMI_NUM_AUDIO_BUFFERS 64

#define THING_ELAST 1.00
#define FRICT_FLOOR 0.60
#define INPUT_FORCE 1.00
#define NPCIN_FORCE 0.80
#define JUMPN_FORCE 0.05
#define GRAVT_FORCE 0.20

#define NO_TILE   0
#define FLAT_TILE 1
#define HIGH_TILE 2

#define TILE_W 20.0f
#define TILE_H 20.0f
#define HALF_TILE_W 10.0f
#define HALF_TILE_H 10.0f

#define FLOOR_W 8
#define FLOOR_H 8
#define FLOOR_SIZE 64

#endif

#ifdef __KAMI_DEBUG__
//print out a vector, normal, point or vertex
#define PRINT_VECT(X) std::cout << "<" << X.x << ", " << X.y << ", " << X.z << ">" << std::endl
//when an if statement fails, print out a message (M) and value (V)
#define ELSE_CERR(M, V) else { std::cerr << M << V << std::endl; }
#else // if notdef __KAMI__DEBUG__
// we don't compile debug out lines
#define PRINT_VECT(X) //
#define ELSE_CERR(M, V) //
#endif

//includes needed for debugging
#include <assert.h>
#include <iostream>
#include <string>

//Platform specific includes

#include <GLUT/GLUT.h>
#include <OpenGL/gl.h>

