//
//  math.cpp
//  kami
//
//  Created by John Evans Wagenaar on 2012/04/07.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include "../base/kami.h"
#include "../base/kamimath.h"

using namespace kami;

/* == VECTOR BASED FUNCTIONS ==*/

/* ============================================ */
float vmath::dot(const vector &a, const vector &b)
{
    return a.x*b.x + a.y*b.y + a.z*b.z;
}

/* =========================== */
float vmath::len(const vector &v)
{
    return sqrtf(dot(v, v));
}

/* ================================ */
float vmath::calc_rot(const vector &v)
{
    float rot = 0.0;
    if(v.x == 0.0 && v.y > 0) {
        rot = 0.0;
    } else if(v.x == 0.0 && v.y < 0.0) {
        rot = 180.0;
    } else if(v.y == 0.0 && v.x > 0.0) {
        rot = 270.0;
    } else if(v.y == 0.0 && v.x < 0.0) {
        rot = 90.0;
    } else if(v.y != 0.0 && v.x != 0.0) {
        float tan_theta = v.x / v.y;
        if(v.x > 0.0) {
            rot = atan(tan_theta)*180/3.145 - 90.0;
        } else {
            rot = atan(tan_theta)*180/3.145 + 90.0;
        }
    }
    return rot;
}

/* ================================== */
vector vmath::normalize(const vector &v)
{
    float len = vmath::len(v);
    vector result = {0.0, 0.0, 0.0};
    if(len > 0.0) {
        result.x = v.x/len;
        result.y = v.y/len;
        result.z = v.z/len;
    }
    return result;
}

/* ============================================= */
vector vmath::add(const vector &a, const vector &b)
{
    vector result = {a.x + b.x, a.y + b.y, a.z + b.z};
    return result;
}

/* ============================================= */
vector vmath::sub(const vector &a, const vector &b)
{
    vector result = {a.x - b.x, a.y - b.y, a.z - b.z};
    return result;
}

/* ============================= */
vector vmath::flip(const vector &v)
{
    vector result = {-v.x, -v.y, -v.z};
    return result;
}

/* ============================================== */
vector vmath::smult(const float &s, const vector &v)
{
    vector result = {v.x*s, v.y*s, v.z*s};
    return result;
}

/* =============================================== */
vector vmath::cross(const vector &a, const vector &b)
{
    vector result = {
        a.y*b.z - b.y*a.z,
        b.x*a.z - a.x*b.z,
        a.x*b.y - b.x*a.y
    };
    return result;
}

/* ============================================== */
vector vmath::proj(const vector &a, const vector &b)
{
    float  v_len  = proj_len(a, b);
    vector result = smult(v_len, normalize(b));
    
    return result;
}

/* ================================================= */
float vmath::proj_len(const vector &a, const vector &b)
{
    float denom  =  vmath::len(b);
    float result =  dot(a, b) / denom;
    
    if(result < 0.0) {
        result = -result;
    }
    
    return result;
}

/* ================================================= */
point vmath::translate(const point &p, const vector &v)
{
    point result = {
        p.x + v.x,
        p.y + v.y,
        p.z + v.z
    };
    return result;
}

/* ========================================================================== */
point pmath::point_on_plane(const point &p, const normal &n, const float &delta)
{
    point result = {
        p.x - n.x * delta,
        p.y - n.y * delta,
        p.z - n.z * delta
    };
    return result;
}

/* == POINT BASED FUNCTIONS ==*/

/* =========================== */
point pmath::flip(const point &p)
{
    point result = { -p.x, -p.y, -p.z };
    return result;
}

/* =========================================== */
float pmath::dist(const point &a, const point &b)
{
    float x = a.x - b.x;
    float y = a.y - b.y;
    float z = a.z - b.z;
    
    float r2 = x*x + y*y + z*z;
    
    return sqrtf(r2);
}

/* =================================================== */
point pmath::translate(const point &p, const vector &dir)
{
    point result = {
        p.x + dir.x,
        p.y + dir.y,
        p.z + dir.z
    };
    return result;
}

/* ========================================================= */
vector pmath::vector_from_point(const point &a, const point &b)
{
    vector result = {
        b.x - a.x,
        b.y - a.y,
        b.z - a.z
    };
    return result;
}

/* ============================================================= */
vector pmath::vector_from_point(const point2d &a, const point2d &b)
{
    vector result = {
        b.x - a.x,
        b.y - a.y,
        0.0
    };
    return result;
}

