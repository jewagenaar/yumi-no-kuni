//
//  kamimodel.h
//  kami
//
//  Created by John Evans Wagenaar on 2012/05/30.
//  Copyright (c) 2012 John-Evans Wagenaar. All rights reserved.
//

#ifndef kami_kamimodel_h
#define kami_kamimodel_h

#include "primitives.h"
#include <vector>

namespace kami {    
    // 3DS Model data
    typedef struct t_m3ds {
        char name[20];
        
        unsigned short vertex_count;
        unsigned short polygon_count;
        unsigned short uv_count;
        
        vertex  *vertices;
        polygon *polygons;
        texco   *uv_coords;
    } m3ds;
    
    // union to enable switching between different types Wavefront OBJ faces
    typedef union t_wfobj_face {
        short face_3vpfnt [6];  // 3 vertices per face, no texco
        short face_3vpf   [9];  // 3 vertices per face w. texco
        short face_4vpf   [12]; // 4 vertices per face w. texco
    } wfobj_face;
    
    // wavefront object data
    typedef struct t_wfobj {
        std::vector<vertex>     vertices;
        std::vector<normal>     normals;
        std::vector<texco>      texcos;
        std::vector<wfobj_face> faces;
    } wfobj;
    
    // Quake MD2 header
    typedef struct md2_header_t {
        int ident;         // magic number: "IDP2"
        int version;       // version: must be 8
        int skinwidth;     // texture width
        int skinheight;    // texture height
        int framesize;     // size in bytes of a frame
        int num_skins;     // number of skins
        int num_vertices;  // number of vertices per frame
        int num_st;        // number of texture coordinates
        int num_tris;      // number of triangles
        int num_glcmds;    // number of opengl commands
        int num_frames;    // number of frames
        int offset_skins;  // offset skin data
        int offset_st;     // offset texture coordinate data
        int offset_tris;   // offset triangle data
        int offset_frames; // offset frame data
        int offset_glcmds; // offset OpenGL command data
        int offset_end;    // offset end of file
    } md2_header;
    
    // MD2 vector
    typedef float md2_vector [3];
    
    // MD2 texture name
    typedef struct md2_skin_t {
        char name[64]; // texture file name
    } md2_skin;
    
    // MD2 texture coords
    typedef struct md2_texcoord_t {
        short s;
        short t;
    } md2_texcoord;
    
    // MD2 triangle
    typedef struct md2_triangle_t {
        unsigned short vertex[3]; // vertex indices of the triangle
        unsigned short st[3];     // tex coord indices
    } md2_triangle;
    
    // MD2 compressed vertex
    typedef struct md2_vertex_t {
        unsigned char v[3];        // position 
        unsigned char normalIndex; // normal vector index 
    } md2_vertex;
    
    // MD2 keyframe
    typedef struct md2_frame_t {
        char name[16];        // frame name
        md2_vector scale;     // scale factor
        md2_vector translate; // translation vector
        md2_vertex *verts;    // list of frame's vertices 
    } md2_frame;
    
    // MD2 model data
    typedef struct t_md2 {
        md2_header    header;
        md2_skin     *skins;
        md2_frame    *frames;
        md2_texcoord *texcoords;
        md2_triangle *triangles;
        int          *gl_commands;
    } md2;
}

#endif
