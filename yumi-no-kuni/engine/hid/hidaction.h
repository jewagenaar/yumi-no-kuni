#ifndef kami_hidaction_h
#define kami_hidaction_h

#include <map>
#include <stdint.h>

#define KEY_W   0x000D
#define KEY_S   0x0001
#define KEY_A   0x0000
#define KEY_D   0x0002
#define KEY_ESC 0x0035
#define KEY_ENT 0x0024
#define KEY_Q   0x000C
#define KEY_I   0x0022

#define KEY_UP  0x007E
#define KEY_DWN 0x007D
#define KEY_LFT 0x007B
#define KEY_RHT 0x007C

#define KEY_SQL 0x0021
#define KEY_SQR 0x001E
#define KEY_SPC 0x0031

namespace kami
{
    class hidaction
    {
        std::map<uint16_t, uint16_t> m_mappings; // key mappings
        
    public:
        static const uint16_t NONE    = 0x0000;
        
        static const uint16_t MV_UP   = 0x0001;
        static const uint16_t MV_DWN  = 0x0002;
        static const uint16_t MV_LFT  = 0x0004;
        static const uint16_t MV_RHT  = 0x0008;
        
        static const uint16_t SHT_UP  = 0x0010;
        static const uint16_t SHT_DWN = 0x0020;
        static const uint16_t SHT_LFT = 0x0040;
        static const uint16_t SHT_RHT = 0x0080;
        
        static const uint16_t JUMP    = 0x0100;
        static const uint16_t ESC     = 0x0200; // ??
        static const uint16_t PAUSE   = 0x0400;
        static const uint16_t USE     = 0x0800;
        
        static const uint16_t INV_LFT = 0x1000;
        static const uint16_t INV_RHT = 0x2000;
        
        hidaction();
        
        uint16_t get_action (const uint16_t &key);
        void     set_action (const uint16_t &key, const uint16_t &action);
    };
}



#endif
