#include <iostream>
#include <stdint.h>
#include "../base/kami.h"
#include "hidaction.h"

#ifdef __KAMI_DEBUG__ 
#include <iostream>
#endif

using namespace kami;

hidaction::hidaction()
{
    m_mappings[KEY_W] = MV_UP;
    m_mappings[KEY_S] = MV_DWN;
    m_mappings[KEY_A] = MV_LFT;
    m_mappings[KEY_D] = MV_RHT;
    
    m_mappings[KEY_UP]  = SHT_UP;
    m_mappings[KEY_DWN] = SHT_DWN;
    m_mappings[KEY_LFT] = SHT_LFT;
    m_mappings[KEY_RHT] = SHT_RHT;
    
    m_mappings[KEY_ESC] = PAUSE;

    m_mappings[KEY_SQL] = INV_LFT;
    m_mappings[KEY_SQR] = INV_RHT;

    m_mappings[KEY_ENT] = USE;
}

/* ============================================= */
uint16_t hidaction::get_action(const uint16_t &key)
{
    return m_mappings[key];
}

/* ================================================================= */
void hidaction::set_action(const uint16_t &key, const uint16_t &action)
{
    m_mappings[key] = action;
}


