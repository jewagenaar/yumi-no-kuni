//
//  hidsys.h
//  kami
//
//  Created by John Evans Wagenaar on 2012/03/21.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef kami_hidsys_h
#define kami_hidsys_h

#include <stdint.h>
#include <queue>
#include "hidaction.h"

/*
 * Human Input Device system
 */

namespace kami
{
    // input event
    typedef struct t_hid_event
    {
        uint16_t mouse_x;
        uint16_t mouse_y;
        uint16_t action;
    } hid_event;
    
    // HID system
    class hidsystem
    {
        hidaction m_mapper;
        
        std::queue<uint8_t> m_inputqueue;
        
        uint16_t m_pause_timeout;
        
        uint16_t m_lastx;
        uint16_t m_lasty;
        
    public:
        uint16_t keystate;
        
        hidsystem();
        
        void mouse_click (const int &button, const uint16_t &x, const uint16_t &y);
        void mouse_move  (const int &x, const int &y);
        
        void key_up   (const uint16_t &key);
        void key_down (const uint16_t &key);
        
        hid_event process_mouse();
    };
}




#endif
