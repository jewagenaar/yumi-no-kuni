#include <iostream>
#include "kami.h"
#include "hidsys.h"

#include <iostream>

using namespace kami;

hidsystem::hidsystem()
{
    m_lastx  = 0;
    m_lasty  = 0;
    keystate = hidaction::NONE;
}

/* ============================================================================== */
void hidsystem::mouse_click(const int &button, const uint16_t &x, const uint16_t &y)
{

}
/* ================================================== */
void hidsystem::mouse_move(const int & x, const int & y)
{
    
}

/* ===================================== */
void hidsystem::key_up(const uint16_t &key)
{
    uint16_t action = m_mapper.get_action(key);
    if (action) {
        keystate ^= action;
    } 
}

/* ======================================= */
void hidsystem::key_down(const uint16_t &key)
{
    // m_inputqueue.push(key);
    uint16_t action = m_mapper.get_action(key);
    
    if (action) {
        keystate |= action;
    }
}

/* ============================== */
hid_event hidsystem::process_mouse()
{
    hid_event result;
    result.mouse_x = m_lastx;
    result.mouse_y = m_lasty;
    
    //process mouse click here
    
    return result;
}
