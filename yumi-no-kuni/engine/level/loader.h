#ifndef __kami__resource_loader__
#define __kami__resource_loader__

#include <iostream>
#include <math.h>

#include "modelman.h"
#include "texman.h"
#include "shaderman.h"
#include "gamemeta.h"
#include "sector.h"

namespace kami
{
    class loader
    {
        npc_meta    m_mastermind;
        npc_meta    m_mancubus;
        
        sector_meta m_sector;
        avatar_meta m_avatar;
        
        
        
        std::string resource_dir;
        
    public:
        static loader *instance();
        
        ~loader();
        
        void set_resource_path(const char* path);
        
        // return the start sector of the level
        sector *demo_level();
        
        avatar *demo_avatar();
        
    protected:
        loader();
        
        void load();
        void configure();
        void build();
        
        sector *parse_plan(const floor_plan plan);
        
        sector *create_random(const uint8_t &type);
    };
}



#endif /* defined(__kami__resource_loader__) */
