#include "kami.h"
#include "audioenv.h"
#include "loader.h"
#include "bboxman.h"
#include "mastermind.h"
#include "mancubus.h"
#include "avatar.h"
#include "sector.h"
#include "item.h"
#include "imgman.h"
#include "global_ids.h"
#include <OpenAL/al.h>

using namespace kami;

vector *UP_TYPE_1 = new vector( { 0.0, 0.0, 1.0 } );

point TILE_TYPE_1 [4] = {
    {  TILE_W/2, -TILE_H/2, 0.0f },
    {  TILE_W/2,  TILE_H/2, 0.0f },
    { -TILE_W/2,  TILE_H/2, 0.0f },
    { -TILE_W/2, -TILE_H/2, 0.0f }
};

floor_plan demo = {
    0, 9, 9, 0, 0, 9, 9, 0,
    9, 9, 0, 9, 9, 9, 9, 0,
    0, 9, 0, 9, 0, 9, 9, 0,
    0, 9, 9, 9, 9, 9, 9, 0,
    0, 9, 9, 0, 0, 9, 9, 0,
    9, 9, 0, 9, 0, 9, 9, 0,
    0, 9, 0, 9, 0, 9, 9, 0,
    0, 9, 9, 9, 9, 9, 9, 0
};

/* ===================== */
loader * loader::instance()
{
    static loader * __instance = new loader();
    return __instance;
}

/* ========== */
loader::loader()
{

}

/* =========== */
loader::~loader()
{
    
}

void loader::set_resource_path(const char *path)
{
    resource_dir = std::string(path); // set the resource folder
    
    load();      // load resources from disk
    configure(); // configure the metadata model
    build();     // build the level from the metamodel and resources
}

/* ====================== */
sector *loader::demo_level()
{
    sector *_sector = parse_plan(demo);
    
    return _sector;
}

/* ======================= */
avatar *loader::demo_avatar()
{
    avatar *_avatar =
    new (std::nothrow) avatar(modelman::instance()->get_md2model(m_avatar.mm.key));
    _avatar->set_position(m_avatar.pos);
    _avatar->rot.roll = 180.0f;
    
    vector adj = { 0.0f, 0.0f, 1.2f };
    _avatar->set_model_adj(adj);
    
    return _avatar;
}

/* ============= */
void loader::load()
{
    // load the models
    modelman::instance()->load_md2(resource_dir + "/models/tris.md2", 0);
    modelman::instance()->load_md2(resource_dir + "/models/mastermind.md2", 1);
    modelman::instance()->load_wfo(resource_dir + "/models/room_type_9.obj", 2);
    modelman::instance()->load_wfo(resource_dir + "/models/skydome.obj", 3);
    modelman::instance()->load_md2(resource_dir + "/models/mancubus.md2", 4);
    modelman::instance()->load_wfo(resource_dir + "/models/item_prop.obj", MODEL_ITEM);
    
    // load some non-gl images
    texman::instance()->load_texture(resource_dir + "/textures/status_hud.png", IMG_STATUSHUD);
    texman::instance()->load_texture(resource_dir + "/textures/menu_newgame.png", IMG_MENUNEWGAME);
    texman::instance()->load_texture(resource_dir + "/textures/menu_options.png", IMG_MENUOPTIONS);
    texman::instance()->load_texture(resource_dir + "/textures/menu_debug.png", IMG_MENUDEBUG);
    texman::instance()->load_texture(resource_dir + "/textures/menu_quit.png", IMG_MENUQUIT);
    texman::instance()->load_texture(resource_dir + "/textures/menu_back.png", IMG_MENUBACK);
    texman::instance()->load_texture(resource_dir + "/textures/angry.png", IMG_MENUCURSOR);
    texman::instance()->load_texture(resource_dir + "/textures/status_inventory.png", IMG_INVENTORY);
    texman::instance()->load_texture(resource_dir + "/textures/inv_potion.png", IMG_POTION);
    texman::instance()->load_texture(resource_dir + "/textures/inv_crystal.png", IMG_CRYSTAL);
    texman::instance()->load_texture(resource_dir + "/textures/inv_food.png", IMG_FOOD);
    
    // create some gl textures
    texman::instance()->load_texture(resource_dir + "/textures/tris.png", 0);
    texman::instance()->load_texture(resource_dir + "/textures/ground.png", 1);
    texman::instance()->load_texture(resource_dir + "/textures/skydome.png", 2);
    texman::instance()->load_texture(resource_dir + "/textures/mastermind.png", 3);
    texman::instance()->load_texture(resource_dir + "/textures/metal.png", 4);
    texman::instance()->load_texture(resource_dir + "/textures/mancubus.png", 5);
    texman::instance()->load_texture(resource_dir + "/textures/inv_food.png", TEX_CRYSTAL);
    
    // now load bboxes
    modelman::instance()->load_wfo(resource_dir + "/models/room_base_col.obj", KAMI_BBOX_MODEL);
    wfobj *bbox_obj = modelman::instance()->get_wfo(KAMI_BBOX_MODEL);
    bboxman::instance()->create(bbox_obj, 0);
    modelman::instance()->free_wfmodel(KAMI_BBOX_MODEL);
    
    // load the font texture
    texman::instance()->load_texture(resource_dir + "/textures/font-courier.png", KAMI_DEBUG_FONT);
    
    // load the shaders
    shaderman::instance()->setup(resource_dir + "/shaders/environment.vert",
                                 resource_dir + "/shaders/environment.frag", 1);
    shaderman::instance()->setup(resource_dir + "/shaders/avatar.vert",
                                 resource_dir + "/shaders/avatar.frag", 2);
    shaderman::instance()->setup(resource_dir + "/shaders/npc.vert",
                                 resource_dir + "/shaders/npc.frag", 3);
    shaderman::instance()->setup(resource_dir + "/shaders/skydome.vert",
                                 resource_dir + "/shaders/skydome.frag", 4);
    
    // load some audio files
    ALuint zoomc = audioenv::env()->load(resource_dir + "/audio/zoomc.wav", true);
    //audioenv::env()->enqueue(zoomc);
    
    ALuint random_lfo = audioenv::env()->load(resource_dir + "/audio/random_lfo.wav", false);
    //audioenv::env()->enqueue(random_lfo);
}

/* ================== */
void loader::configure()
{
    static int ID = 0;
    
    // create a mastermind meta model
    m_mastermind.id                 =  ID++;
    
    m_mastermind.mm.key             =  ID++;
    m_mastermind.mm.data_key        =  1;
    m_mastermind.mm.texmap_key      =  3;
    m_mastermind.mm.shader_key      =  3;
    
    // create a mancubus meta model
    m_mancubus.id                 =  ID++;
    
    m_mancubus.mm.key             =  ID++;
    m_mancubus.mm.data_key        =  4;
    m_mancubus.mm.texmap_key      =  5;
    m_mancubus.mm.shader_key      =  3;
    
    // create a sector meta model
    m_sector.id                    = ID++;
    
    m_sector.render_mm.key         = 9;
    m_sector.render_mm.data_key    = 2;
    m_sector.render_mm.texmap_key  = 1;
    m_sector.render_mm.shader_key  = 1;
    
    m_sector.skydome_mm.key        = ID++;
    m_sector.skydome_mm.data_key   = 3;
    m_sector.skydome_mm.texmap_key = 2;
    m_sector.skydome_mm.shader_key = 4;
    
    // create a level meta model
    m_avatar.id             = ID++;
    
    m_avatar.mm.key         = ID++;
    m_avatar.mm.data_key    = 0;
    m_avatar.mm.texmap_key  = 0;
    m_avatar.mm.shader_key  = 2;
    
    m_avatar.pos.x          =  0.0f;
    m_avatar.pos.y          =  0.0f;
    m_avatar.pos.z          = 10.0f;
}

/* ============== */
void loader::build()
{
    modelman::instance()->create_md2model(m_avatar.mm);
    modelman::instance()->create_md2model(m_mastermind.mm);
    modelman::instance()->create_md2model(m_mancubus.mm);
    
    modelman::instance()->create_wfmodel (m_sector.skydome_mm);
    modelman::instance()->create_wfmodel (m_sector.render_mm);
}

/* =========================================== */
sector *loader::parse_plan(const floor_plan plan)
{
    std::unordered_map<uint8_t, sector*> sectors;
    
    uint8_t row_count;
    uint8_t col_count;
    
    uint8_t f = 0;
    
    // find the first tile
    for (int i = 0; i < FLOOR_SIZE; i++) {
        if (plan[i] != 0) {
            f = i;
            break;
        }
    }
    
    // now create the tile map
    for (int i = f; i < FLOOR_SIZE; i++) {
        
        col_count = i % FLOOR_H;
        row_count = i / FLOOR_W;
        
        uint8_t t = plan[i];
        
        // start int top right corner (start of array) and link in north and west directions
        // moving towards the bottom right corner (end of the array)
        
        if (t != 0) {
            
            sector *new_sector = create_random(t);
            
            if (row_count != 0) {
                uint8_t ni = i - FLOOR_W;
                sector *temp = sectors[ni];
                if(temp != 0) {
                    new_sector->north = temp;
                    temp->south       = new_sector;
                }
            }
            
            if (col_count != 0) {
                uint8_t ni = i - 1;
                sector *temp = sectors[ni];
                if(temp != 0) {
                    new_sector->west = temp;
                    temp->east       = new_sector;
                }
            }
            
            sectors[i] = new_sector;
        }
    }
    
    // return the first sector
    return sectors[f];
}

/* ============================================ */
sector *loader::create_random(const uint8_t &type)
{
    sector *result = new sector;
    
    wfmodel *model    = modelman::instance()->get_wfmodel(type);
    bbox    *col_data = bboxman::instance()->get_bbox(0);
    
    md2model *mastermind_model = modelman::instance()->get_md2model(m_mastermind.mm.key);
    md2model *mancubus_model   = modelman::instance()->get_md2model(m_mancubus.mm.key);
    
    result->set_renderable(model);
    result->points   = TILE_TYPE_1;
    result->up_dir   = UP_TYPE_1;
    result->col_data = col_data;
    
    int n = rand() % 10;
    
    for(int i = 0; i < n; i++) {
        point npc_pos = {
            10.0f - (float) (rand() % 20),
            10.0f - (float) (rand() % 20),
            20.0f - (float) (rand() % 10)
        };
        
        if(i % 2) {
            
            mastermind *_mastermind =
            new (std::nothrow) mastermind(mastermind_model);
            _mastermind->set_position(npc_pos);
            _mastermind->inventory[ITEM_COIN]++;
            
            result->npcs.push_back(_mastermind);
        } else {
            mancubus *_mancubus =
            new (std::nothrow) mancubus(mancubus_model);
            _mancubus->set_position(npc_pos);
            _mancubus->inventory[ITEM_POTION]++;
            
            result->npcs.push_back(_mancubus);
        }
    }
    
    return result;
}
