#ifndef __yumi_no_kuni__hithandler__
#define __yumi_no_kuni__hithandler__

#include "kami.h"

namespace kami
{
    template<class obj>
    class hithandler 
    {
    public:
        virtual void handle (obj *o) = 0;
    };
}

#endif /* defined(__yumi_no_kuni__hithandler__) */
