//
//  world.h
//  kami
//
//  Created by John Evans Wagenaar on 2012/03/28.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef kami_world_h
#define kami_world_h

#include "kami.h"
#include "primitives.h"
#include "scene.h"
#include "thing.h"
#include "pdvobject.h"
#include "sector.h"
#include "projectile.h"

#include <vector>

namespace kami
{
    class world
    {        
        vector m_up;
        vector m_gravity;
        
    public:
        avatar *the_avatar;
        sector *current_sector;
        
        world();
        
        void step  (const float &t);
        
    protected:
        // top level entry method to check collision for thing
        void process_collisions (const float &t, thing *to);
        
        // check collision of thing against wall
        void walls_collision (const float &t, thing *to);
        // check collision of thing against floor
        void floor_collision (const float &t, thing *to);
        // check collision of thing against other things
        void thing_collision (const float &t, thing *to);
        // check collision of thing against projectiles
        void proj_collision  (const float &t, thing *to);
        // check collision of thing against props
        void prop_collision  (const float &t, thing *to);
        
        void add_gravity     (const float &t, thing *to);
        void add_friction    (const float &t, thing *to);
        
        void update_velocity (const float &t, thing *to);

        void check_proj_walls (const float &t, projectile *proj);
    };
}



#endif
