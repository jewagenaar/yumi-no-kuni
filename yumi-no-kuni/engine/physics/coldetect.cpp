//
//  coldetect.cpp
//  kami
//
//  Created by John Evans Wagenaar on 2012/06/08.
//  Copyright (c) 2012 John-Evans Wagenaar. All rights reserved.
//

#include <iostream>
#include <ctime>
#include "coldetect.h"

using namespace kami;

/* ================================================================ */
colev coldetect::check_thing_vs_bbox(const colthing *thing, bbox *box)
{
    colev result;
    colev ev;
    
    result.hit   = false;
    result.delta = 0.0;
    ev.hit       = false;
    uint32_t i   = 0;
    
    while(i < box->num_tris) {
        coltri current = box->triangles[i];
        
        ev = check_thing_vs_triangle(thing, &current);
        if(ev.hit && ev.delta < result.delta) {
            result = ev;
        }
        ++i;
    }
    
    return result;
}

/* ====================================================================== */
colev coldetect::check_thing_vs_triangle(const colthing *thing, coltri *tri)
{
    colev  ev; ev.hit = false;
    vector disp       = vmath::smult(-thing->r, tri->n);
    point  end        = pmath::translate(thing->end,   disp);
    
    // check if thing is moving away from the triangle
    float f = vmath::dot(tri->n, thing->dir);
    if (f > 0.0) {
        return ev;
    }
    
    // check if the thing started behind the plane
    float start_d = check_point_vs_plane(thing->start, tri);
    if (start_d < 0.0) {
        return ev;
    }
    
    // check if the end position is in front of the plane
    float end_d = check_point_vs_plane(end, tri);
    if (end_d > 0.0) {
        return ev;
    }
    
    // there was a collision with the triangle's plane
    // check if the point of collision (p) is inside the triangle
    point p = pmath::point_on_plane(end, tri->n, end_d);
    
    if (check_point_vs_triangle(p, tri)) {
        // there is a collision inside the triangle
        ev.hit   = true;
        ev.delta = end_d;
        ev.n     = tri->n;
        ev.v     = tri->vector_p;
        ev.p     = p;
    } else {
        // check collision with one of the edges
        ev = check_thing_vs_edges(thing, tri);
    }
    
    return ev;
}

/* ============================================================== */
bool coldetect::check_point_vs_triangle(const point &p, coltri *tri)
{
    //NB: the point (p) most be on the same plane as the triangle, otherwise
    //    this function will give inaccurate results
    
    //check if the cross product for each vector pair points in the
    //same direction, if they do, we have a collision
    vector a_p = {
        tri->vertex_a.x - p.x,
        tri->vertex_a.y - p.y,
        tri->vertex_a.z - p.z
    };
    
    vector b_p = {
        tri->vertex_b.x - p.x,
        tri->vertex_b.y - p.y,
        tri->vertex_b.z - p.z
    };
    
    vector c_p = {
        tri->vertex_c.x - p.x,
        tri->vertex_c.y - p.y,
        tri->vertex_c.z - p.z
    };
    
    vector cross_a = vmath::cross(tri->vector_p, a_p);
    vector cross_b = vmath::cross(tri->vector_q, b_p);
    vector cross_c = vmath::cross(tri->vector_r, c_p);
    
    bool result =
    (vmath::dot(cross_a, cross_b) >= 0.0) &&
    (vmath::dot(cross_b, cross_c) >= 0.0) &&
    (vmath::dot(cross_c, cross_a) >= 0.0);
    
    return result;
    
}

/* ============================================================ */
float coldetect::check_point_vs_plane(const point &p, coltri *tri)
{
    
    // a point (p) in the plane must conform to 0 = dot(n, v - v0)
    vector v0 = {
        tri->vertex_a.x - p.x,
        tri->vertex_a.y - p.y,
        tri->vertex_a.z - p.z
    };
    
    vector v_v0 = {
        tri->vector_p.x - v0.x,
        tri->vector_p.y - v0.y,
        tri->vector_p.z - v0.z
    };
    
    //calculate how far the point is beyond the plane
    float  delta = vmath::dot(tri->n, v_v0);
    
    return delta;
}

/* =================================================================== */
sector *coldetect::check_thing_vs_tile_bounds(colthing *thing, sector *s)
{
    sector *result = s;
    
    if (thing->end.x > HALF_TILE_W) {
        if (s->east) {
            thing->start.x -= TILE_W;
            thing->end.x   -= TILE_W;
            result = s->east;
        } else {
            thing->start.x = HALF_TILE_W;
            thing->end.x   = HALF_TILE_W;
        }
    }
    
    if (thing->end.x < -HALF_TILE_W) {
        if (s->west) {
            thing->start.x += TILE_W;
            thing->end.x   += TILE_W;
            result = s->west;
        } else {
            thing->start.x = -HALF_TILE_W;
            thing->end.x   = -HALF_TILE_W;
        }
    }
    
    if (thing->end.y > HALF_TILE_H) {
        if (s->north) {
            thing->start.y -= TILE_H;
            thing->end.y   -= TILE_H;
            result = s->north;
        } else {
            thing->start.y = HALF_TILE_H;
            thing->end.y   = HALF_TILE_H;
        }
    }
    
    if (thing->end.y < -HALF_TILE_H) {
        if (s->south) {
            thing->start.y += TILE_H;
            thing->end.y   += TILE_H;
            result = s->south;
        } else {
            thing->start.y = -HALF_TILE_H;
            thing->end.y   = -HALF_TILE_H;
        }
    }
    
    return result;
}

/* =============================================================== */
bool coldetect::check_proj_vs_tile_bounds(colthing *thing, sector *s)
{    
    if (thing->end.x > HALF_TILE_W  ||
        thing->end.x < -HALF_TILE_W ||
        thing->end.y > HALF_TILE_H  ||
        thing->end.y < -HALF_TILE_H) {
        
        return true;
    }
    
    return false;
}

/* ================================================================ */
colev coldetect::check_thing_vs_tile(const colthing *thing, sector *s)
{
    colev ev; ev.hit = false;
    
    float delta = check_point_vs_tile(thing->end, s);
    
    if(delta <= 0) {
        ev.hit   = true;
        ev.delta = delta;
        ev.n     = *(s->up_dir);
        ev.p     = thing->end;
        ev.v     = {
            s->points[0].x - s->points[1].x,
            s->points[0].y - s->points[1].y,
            s->points[0].z - s->points[1].z,
        };
    }
    
    return ev;
}

/* ========================================================= */
float coldetect::check_point_vs_tile(const point &p, sector *s)
{
    vector v = {
        s->points[0].x - p.x,
        s->points[0].y - p.y,
        s->points[0].z - p.z,
    };
    
    vector v0 = {
        s->points[1].x - s->points[0].x,
        s->points[1].y - s->points[0].y,
        s->points[1].z - s->points[0].z,
    };
    
    vector v_v0 = {
        v0.x - v.x,
        v0.y - v.y,
        v0.z - v.z
    };
    
    normal n = *(s->up_dir);
    
    float delta = vmath::dot(n, v_v0);
    
    return delta;
}

/* =================================================================== */
colev coldetect::check_thing_vs_edges(const colthing *thing, coltri *tri)
{
    
    colev ev      = check_point_vs_edge(thing->end, thing->r, tri->edge_a);
    colev next_ev = check_point_vs_edge(thing->end, thing->r, tri->edge_b);
    ev            = next_ev.hit && (next_ev.delta < ev.delta) ? next_ev : ev;
    next_ev       = check_point_vs_edge(thing->end, thing->r, tri->edge_c);
    ev            = next_ev.hit && (next_ev.delta < ev.delta) ? next_ev : ev;
    
    return ev;
}

/* ==================================================================================== */
colev coldetect::check_point_vs_edge(const point &p, const float &r, const coledge & edge)
{
    colev  ev; ev.hit = false;
    vector pq         = pmath::vector_from_point(edge.start, p);
    vector proj_pq    = vmath::proj(pq, edge.dir);
    vector d          = vmath::sub(proj_pq, pq);
    float  len_d      = vmath::len(d);
    
    if (len_d < r) {
        
        // the point is in the edge's infinite cylinder volume
        // next determine if it's in the finite volume
        vector pr = pmath::vector_from_point(p, edge.end);
        
        float dot_a = vmath::dot(pr, edge.dir);
        float dot_b = vmath::dot(pq, edge.dir);
        
        // done by doing a "scewed triangle" check - if it's scew, then there's a collision
        bool hit = (dot_a > 0 && dot_b > 0) ? true : (dot_a < 0.0 && dot_b < 0.0) ? true : false;
        
        if (hit) {
            ev.hit      = true;
            ev.delta    = len_d - r;
            point col_p = pmath::translate(p, d);
            ev.n        = pmath::vector_from_point(col_p, p);
            ev.n        = vmath::normalize(ev.n);
            ev.v        = edge.dir;
            ev.p        = col_p;
        }
    }
    
    return ev;
}

/* ============================================================================== */
scolev coldetect::check_thing_vs_thing(const colthing *thing, const colthing *other)
{

    scolev ev; ev.hit = false;
    
    vector vect     = pmath::vector_from_point(thing->end, other->end);
    float dot_check = vmath::dot(vect, thing->dir);
    
    if (dot_check < 0.0) {
        return  ev;
    }
    
    float min_dist = thing->r + other->r;
    float min_sqr  = min_dist * min_dist;
    float dist_sqr = vmath::dot(vect, vect);
    
    if (dist_sqr < min_sqr) {
        ev.hit   = true;
        ev.dir   = vmath::normalize(vect);
        ev.delta = min_dist - sqrtf(dist_sqr);
    }
    
    return ev;
}

/* ===================================================================== */
bool coldetect::check_thing_vs_prop(const colthing *t, const kami::prop *p)
{
    vector vect = pmath::vector_from_point(t->end, p->pos);

    float min_dist = t->r + p->r;
    float min_sqr  = min_dist * min_dist;
    float dist_sqr = vmath::dot(vect, vect);

    return dist_sqr < min_sqr;
}

