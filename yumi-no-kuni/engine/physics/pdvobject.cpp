//
//  pdvobject.cpp
//  kami
//
//  Created by John-Evans Wagenaar on 2012/09/02.
//  Copyright (c) 2012 John-Evans Wagenaar. All rights reserved.
//

#include "pdvobject.h"
#include "kamimath.h"

using namespace kami;

pdvobject::pdvobject(const kami::colev &ev, const kami::vector &force)
{
    init(ev, force);
}

/* ================================================================ */
void pdvobject::init(const kami::colev &ev, const kami::vector &force)
{
    kami::vector v1 = vmath::smult(3.0f, vmath::normalize(ev.v));
    kami::vector v2 = vmath::cross(ev.n, v1);
    
    p1 = ev.p;
    p2 = pmath::translate(p1, v1);
    p3 = pmath::translate(p1, v2);
    
    pn = pmath::translate(p1, vmath::smult(3.0f, ev.n));
    pf = pmath::translate(p1, force);
}

/* ================== */
void pdvobject::render()
{    
    glPushMatrix();
    glUseProgram(0);
    
    glLineWidth(2.0f);
    glBegin(GL_LINES);
    
    glColor3f(1.0, 0.0, 0.0);
    glVertex3f(p1.x, p1.y, p1.z);
    glVertex3f(p2.x, p2.y, p2.z);
    
    glColor3f(0.0, 1.0, 0.0);
    glVertex3f(p1.x, p1.y, p1.z);
    glVertex3f(p3.x, p3.y, p3.z);
    
    glColor3f(0.0, 0.0, 1.0);
    glVertex3f(p1.x, p1.y, p1.z);
    glVertex3f(pn.x, pn.y, pn.z);
    
    glColor3f(0.6, 0.1, 0.6);
    glVertex3f(p1.x, p1.y, p1.z);
    glVertex3f(pf.x, pf.y, pf.z);
    
    glEnd();
    
    glColor3f(0.6, 0.1, 0.6);
    glTranslatef(p1.x, p1.y, p1.z);
    glutSolidSphere(0.5, 8, 8);
    
    glPopMatrix();
}

/* ==================================================================== */
void pdvobject::update_pos(const float &x, const float &y, const float &z)
{
    ASGN_VOP(position, x, y, z);
}
