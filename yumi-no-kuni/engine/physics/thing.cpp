//
//  thing.cpp
//  kami
//
//  Created by John Evans Wagenaar on 2012/03/28.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include "../base/kami.h"
#include "../base/kamimath.h"

#include "thing.h"

using namespace kami;

thing::thing() :
elasticity (THING_ELAST),
colradius  (0.5),
accel      (0.0),
mass       (1.0),
freefall   (true),
handler    (0)
{
    static uint32_t __ID = 0;
    ID = __ID++;
    
    INIT_VOP(direction, 0.0f);
    INIT_VOP(force,     0.0f);
    INIT_VOP(position,  0.0f);
    INIT_VOP(velocity,  0.0f);
}

/* ================================ */
void thing::update_pos(const float &t)
{
    position.x += (velocity.x * t);
    position.y += (velocity.y * t);
    position.z += (velocity.z * t);
}

/* ============================== */
void thing::set_dir(const vector &v)
{
    direction = vmath::normalize(v);
}

/* ============================================================= */
void thing::set_pos(const float &x, const float &y, const float &z)
{
    ASGN_VOP(position, x, y, z);
}

/* ========================================= */
colthing *thing::collision_data(const float &t)
{
    vector disp = vmath::smult     (t, velocity);
    point  end  = pmath::translate (position, disp);
    
    m_colthing.dir   = direction;
    m_colthing.start = position;
    m_colthing.end   = end;
    m_colthing.r     = colradius;
    
    return &m_colthing;
}
