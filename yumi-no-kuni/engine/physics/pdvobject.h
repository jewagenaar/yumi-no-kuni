//
//  pdvobject.h
//  kami
//
//  Created by John-Evans Wagenaar on 2012/09/02.
//  Copyright (c) 2012 John-Evans Wagenaar. All rights reserved.
//

#ifndef kami_pdvobject_h
#define kami_pdvobject_h

#include "../base/primitives.h"
#include "../scene/renderable.h"

#include "coldetect.h"

namespace kami
{
    // Physics Data Visualization (PDV) Object
    class pdvobject : public renderable
    {
        vertex p1;
        vertex p2;
        vertex p3;
        
        vertex pn;
        vertex pf;
        
    public:
        point position;
        
        explicit pdvobject(const colev &ev, const vector &force);
        void render();
        
        void  update_pos(const float &x, const float &y, const float &z);
    protected:
        void init(const colev &ev, const vector &force);
    };
}


#endif
