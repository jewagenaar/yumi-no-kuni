//
//  coldetect.h
//  kami
//
//  Created by John Evans Wagenaar on 2012/06/08.
//  Copyright (c) 2012 John-Evans Wagenaar. All rights reserved.
//

#ifndef kami_coldetect_h
#define kami_coldetect_h

#include <inttypes.h>
#include "thing.h"
#include "colshapes.h"
#include "primitives.h"
#include "kamimath.h"
#include "item.h"
#include "sector.h"

namespace kami
{
    // holds info about a sphere to surface collision
    typedef struct t_col {
        bool   hit;   // if there was a collision this is true
        float  delta; // how far the object is beyond the plane
        vector n;     // plane normal
        vector v;     // a vector in the plane
        point  p;     // the point of contact
    } colev;
    
    // holds info about a sphere to sphere collision
    typedef struct t_scolev {
        bool   hit;   // if there was a collision this is true
        vector dir;   // the direction
        float  delta; // how much the spheres penetrate
    } scolev;
    
    namespace coldetect
    {
        // checks if a moving thing is going to collide with a bbox
        colev check_thing_vs_bbox (const colthing *ct, bbox *box);
        
        // check if a thing is going to collide with a bbox
        colev check_thing_vs_triangle (const colthing *ct, coltri *tri);
        
        // check if a thing is within a triangle's bounds
        bool check_point_vs_triangle (const point &p, coltri * tri);
        
        // check if a point is in front or behind a plane
        float check_point_vs_plane (const point &p, coltri *tri);
        
        // check if a thing is within a sector's bounds 
        sector *check_thing_vs_tile_bounds (colthing *ct, sector *s);
        
        // check if a projectile is within a sector's bounds
        bool check_proj_vs_tile_bounds (colthing *ct, sector *s);
        
        // check if a falling thing is colliding with a tile
        colev check_thing_vs_tile (const colthing *ct, sector *s);
        
        // check if a point is in the tile plane
        float check_point_vs_tile (const point &p, sector *s);
        
        // check if a thing is going to collide with any of the triangle's edges
        colev check_thing_vs_edges (const colthing *thing, coltri * tri);
        
        // check if a point is going to collide with a edge
        colev check_point_vs_edge (const point &p, const float & r, const coledge &edge);
        
        // check if two things collide
        scolev check_thing_vs_thing (const colthing *thing, const colthing *other);
        
        // check if two objects are colliding
        bool check_thing_vs_prop (const colthing *t, const prop *p);
    }
}





#endif
