//
//  colshapes.h
//  kami
//
//  Created by John-Evans Wagenaar on 2012/08/04.
//  Copyright (c) 2012 John-Evans Wagenaar. All rights reserved.
//

#ifndef kami_colshapes_h
#define kami_colshapes_h

#include "primitives.h"

#define COLSHAPE_COMPOSITE 0x01
#define COLSHAPE_SPHERE    0x02
#define COLSHAPE_EDGE      0x03
#define COLSHAPE_CYLINDER  0x04
#define COLSHAPE_TRIANGLE  0x05
#define COLSHAPE_BBOX      0x06
#define COLSHAPE_PLANE     0x07

namespace  kami {
    
    // a composite shape
    class colshape
    {
    public:
        colshape(unsigned short shape_type) : type(shape_type) {}; // 
        unsigned short type;
        unsigned short child_count;
        colshape       *children;
    };
    
    // a collision sphere
    class colsphere : public colshape
    {
    public:
        colsphere() : colshape(COLSHAPE_SPHERE) {};
        float radius;
    };
    
    // a collision edge - note: not a composite
    class coledge
    {
    public:
        vertex start;
        vertex end;
        vector dir;
    };
    
    // a collision plane - note: not a composite
    class colplane
    {
    public:
        vertex pp; // point in the plane
        vector v;  // a vector in the plane
        normal n;  // plane normal
    };
        
    // a collision cylinder
    class colcyl : public colshape
    {
    public:
        colcyl() : colshape(COLSHAPE_CYLINDER) {};
        float  r;     // radius
        point  start; // start point
        point  end;   // end point
        vector dir;   // direction
    };
    
    // a collision detection
    class coltri : public colshape
    {
    public:
        coltri() : colshape(COLSHAPE_TRIANGLE) {};
        // the color used in debug mode
        rgb_color color;
        
        // the vertices at the corners
        vertex    vertex_a;
        vertex    vertex_b;
        vertex    vertex_c;
        
        // the vectors along the sides
        vector    vector_p;
        vector    vector_q;
        vector    vector_r;
        
        // the edges of along the sides
        coledge   edge_a;
        coledge   edge_b;
        coledge   edge_c;
        
        // the face normal
        normal    n;
    };
    
    // Aabounding box
    class bbox
    {
    public:
        uint32_t  num_tris;
        coltri    *triangles;
    };
    
    // holds info about a movement of a thing
    typedef struct t_colthing {
        float  r;      // col radius
        point  start;  // start point
        point  end;    // end point
        vector dir;    // movement direction
    } colthing;
}


#endif
