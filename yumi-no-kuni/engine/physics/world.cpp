//
//  world.cpp
//  kami
//
//  Created by John Evans Wagenaar on 2012/03/28.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include "kami.h"
#include "kamimath.h"
#include "world.h"
#include "coldetect.h"
#include <time.h>

using namespace kami;

world::world() {
    m_up.x = 0.0;
    m_up.y = 0.0;
    m_up.z = 1.0;
    
    m_gravity = vmath::smult(GRAVT_FORCE, vmath::flip(m_up));
}

/* ========================== */
void world::step(const float &t)
{        
    colthing *avatar_thing = the_avatar->physics_thing()->collision_data(t);
    current_sector         = coldetect::check_thing_vs_tile_bounds(avatar_thing, current_sector);
    the_avatar->set_position(avatar_thing->start); // update the avatar's position
    
    // check avatar collision data
    thing *avatar_to = the_avatar->physics_thing();
    process_collisions (t, avatar_to);
    prop_collision     (t, avatar_to);
    
    // check npc collision data
    auto npc_iter = current_sector->npcs.begin();
    for (; npc_iter < current_sector->npcs.end(); npc_iter++) {
        
        npc *_npc = *npc_iter;
        thing *to = _npc->physics_thing();
        
        process_collisions(t, to);
    }
    
    // now check all the projectiles against the walls
    auto proj_iter = current_sector->projectiles.begin();
    for (; proj_iter < current_sector->projectiles.end(); proj_iter++) {
        projectile *proj = *proj_iter;
        check_proj_walls(t, proj);
    }
}

/* ==================================================== */
void world::process_collisions (const float &t, thing *to)
{
    add_gravity     (t, to);
    walls_collision (t, to);
    floor_collision (t, to);
    thing_collision (t, to);
    proj_collision  (t, to);
    add_friction    (t, to);    
    update_velocity (t, to);
}

/* ================================================ */
void world::floor_collision(const float &t, thing *to)
{
    colthing *ct = to->collision_data(t);
    colev ev     = coldetect::check_thing_vs_tile(ct, current_sector);
    
    if (ev.hit) {
        // if something isn't colliding it's in freefall
        to->freefall = false;
        
        vector disp = vmath::smult     (-1.0 * ev.delta, ev.n);
        point  pos  = pmath::translate (ct->end, disp);
        float  len  = vmath::proj_len  (to->force, ev.n);
        
        // correct the position
        to->position = pos;
        
        // calculate the opposing force
        vector opp_force = vmath::smult (len, ev.n);
        to->force        = vmath::add   (to->force, opp_force);
        
    } else {

        // else the object is not touching the floor, so it's in free fall
        to->freefall = true;
        to->position = ct->end;
    }
}

/* ================================================ */
void world::walls_collision(const float &t, thing *to)
{
    colthing *ct = to->collision_data(t);
    colev     ev = coldetect::check_thing_vs_bbox(ct, current_sector->col_data);
    
    if (ev.hit) {        
        vector disp = vmath::smult     (-1.0 * ev.delta, ev.n);
        point  pos  = pmath::translate (ct->end, disp);
        float  len  = vmath::proj_len  (to->force, ev.n);
        
        // correct the position
        to->position = pos;
        
        // calculate the opposing force
        vector opp_force = vmath::smult (len, ev.n);
        to->force        = vmath::add   (to->force, opp_force);
    }
}


/* ================================================ */
void world::thing_collision(const float &t, thing *to)
{
    auto iter = current_sector->npcs.begin();
    for (; iter < current_sector->npcs.end(); iter++) {
        
        npc *_npc = *iter;
        thing *other = _npc->physics_thing();
        
        if (other->ID == to->ID) {
            continue;
        }
        
        colthing *col_thing = to->collision_data(t);
        scolev    ev =
        coldetect::check_thing_vs_thing(col_thing, other->collision_data(t));
        
        if (ev.hit) {
            point pos    = vmath::translate(col_thing->end, vmath::smult(-ev.delta, ev.dir));
            to->position = pos;
        }
    }
}

/* ================================================ */
void world::proj_collision (const float &t, thing *to)
{
    auto iter = current_sector->projectiles.begin();
    for (; iter < current_sector->projectiles.end(); iter++) {
        
        projectile *proj = *iter;
        
        // disable friendly fire
        if (proj->owner_id == to->ID) {
            continue;
        }
        
        colthing *col_thing = proj->collision_data(t);
        scolev    ev =
        coldetect::check_thing_vs_thing (col_thing, to->collision_data(t));
        
        if (ev.hit && to->handler) {
            to->handler->handle(proj);
        }
    }
}

/* ================================================ */
void world::prop_collision (const float &t, thing *to)
{
    colthing *col_thing = to->collision_data (t);
    
    auto iter = current_sector->props.begin();
    for (; iter < current_sector->props.end(); iter++) {
        prop *p = *iter;
        p->hit = coldetect::check_thing_vs_prop(col_thing, p);
    }
}

/* ============================================= */
void world::add_friction(const float &t, thing *to)
{
    vector force = to->force;
    
    float s = vmath::len(to->velocity);
    float k = FRICT_FLOOR * s;
    
    force.x -= k * force.x;
    force.y -= k * force.y;
    
    if (!to->freefall) {
        force.z -= k * force.z;
    }
    
    to->force = force;
}

/* ============================================ */
void world::add_gravity(const float &t, thing *to)
{
    // get the current forces that are excerted on the thing
    vector current = to->force;
    
    // add some gravity
    vmath::inline_add(current, m_gravity);
    
    to->force = current;
}

/* ================================================ */
void world::update_velocity(const float &t, thing *to)
{
    vector force    = to->force;
    vector velocity = vmath::smult(t/to->mass, force);
    
    // attenuate velocity
    velocity.x = fabs(velocity.x) < 0.05f ? 0.0f : velocity.x;
    velocity.y = fabs(velocity.y) < 0.05f ? 0.0f : velocity.y;
    velocity.z = fabs(velocity.z) < 0.05f ? 0.0f : velocity.z;
    
    to->velocity = velocity;
    to->set_dir(velocity);
}

/* ===========================================d============== */
void world::check_proj_walls (const float &t, projectile *proj)
{
    colthing *ct = proj->collision_data(t);
    colev     ev = coldetect::check_thing_vs_bbox(ct, current_sector->col_data);
    
    if (ev.hit) {
        proj->hit  = true;
    } else {
        proj->hit |= coldetect::check_proj_vs_tile_bounds(ct, current_sector);
    }
}


