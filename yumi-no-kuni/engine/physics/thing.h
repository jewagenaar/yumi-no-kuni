#ifndef kami_thing_h
#define kami_thing_h

#include <inttypes.h>
#include <vector>
#include "primitives.h"
#include "colshapes.h"
#include "hithandler.h"
#include "projectile.h"

namespace kami
{
    // physics thing
    class thing
    {
    protected:        
        colthing m_colthing;
        
    public:
        vector    velocity;
        point     position;
        vector    force;
        vector    direction;
        
        float     elasticity;
        float     colradius;
        float     accel;
        float     mass;
        float     freefall;
        
        hithandler<projectile> *handler;
        
        uint32_t ID;
        
        thing();
        
        void update_pos (const float &t);
        void set_dir    (const vector &v);
        void set_pos    (const float &x, const float &y, const float &z);
        
        colthing *collision_data (const float &t);
    };
}



#endif
