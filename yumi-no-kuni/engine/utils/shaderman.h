//
//  shaderman.hpp
//  kami
//
//  Created by John Evans Wagenaar on 2012/05/20.
//  Copyright (c) 2012 John-Evans Wagenaar. All rights reserved.
//

#ifndef kami_shaderman_hpp
#define kami_shaderman_hpp

#include "../base/kami.h"
#include <unordered_map>
#include <string>


namespace kami
{
    class shaderman
    {
        shaderman() {}
        
        std::unordered_map<int, GLuint> m_shaders;
    public:
        static shaderman *instance();
        
        GLuint get_shader(const int &key);
        
        GLuint setup(const std::string & vert_shader,
                     const std::string & frag_shader,
                     const int &key);
        
    protected:
        void errorlog_shader  (GLuint shader);
        void errorlog_program (GLuint program);
    };
}



#endif
