//
//  texman.h
//  kami
//
//  Created by John Evans Wagenaar on 2012/03/24.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef kami_texman_h
#define kami_texman_h

#include <string>
#include <unordered_map>
#include "../base/kami.h"

#define MAX_TEXTURES 128

namespace kami
{
    class texman {
        texman();
        std::unordered_map<uint16_t, GLuint> m_textures;
        GLuint m_texis[MAX_TEXTURES];
        
    public:
        static texman *instance (void);
        
        GLuint load_texture (const uint16_t &img_key, const uint16_t &key);
        GLuint load_texture (bitmap *bmp, const uint16_t &key);
        GLuint load_texture (const std::string &filename, const uint16_t &key);
        
        GLuint get_texture  (const uint16_t &key);
        
        void   free_texture (const uint16_t &key);
        void   free_all();
    };
}




#endif
