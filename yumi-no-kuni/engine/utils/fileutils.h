//
//  fileutils.h
//  kami
//
//  Created by John Evans Wagenaar on 2012/03/17.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef kami_fileutils_h
#define kami_fileutils_h

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>

#include "png.h"
#include "../../libraries/libpng/png.h"
#include "../base/kami.h"
#include "../base/kamimodel.h"
#include "../base/primitives.h"
#include "../physics/coldetect.h"

#include <iostream>
#include <sstream>
#include <fstream>

//Template definitions for Wavefront model files
//Vertex template
#define VERT_TEMPL "v %f %f %f"
//Vertex normal template
#define NORM_TEMPL "vn %f %f %f"
//Texture coordinate template
#define TXCO_TEMPL "vt %f %f"
//Face template - 3 vertices per face, no texco
#define FACE_TEMPL "f %hi//%hi %hi//%hi %hi//%hi"
//Face template - 3 vertices per face w. texco
#define FACE_TEMPL_3VPF "f %hi/%hi/%hi %hi/%hi/%hi %hi/%hi/%hi"
//Face template - 4 vertices per face w. texco
#define FACE_TEMPL_4VPF "f %hi/%hi/%hi %hi/%hi/%hi %hi/%hi/%hi %hi/%hi/%hi"

#define PNG_SIG_SIZE 8
#define BUFFER_SIZE  256

namespace kami
{
    namespace fileutils
    {
        // read the contents of a file into memory
        std::string load_txt  (const std::string &filename);
        
        // read the contents of the bmp file
        bitmap *load_bmp(const std::string &filename);
        
        // read the contents of an md2 file
        md2 *load_md2file(const char * filename);
        
        // read png data
        void read_png_data(png_structp struct_ptr, png_bytep data, png_size_t len);
        
        // load a png image into a bitmap
        bitmap *load_png(const std::string &filename);
        
        // read the contents of a Wavefront model file into a wfobj
        wfobj *load_wfofile(const char * filename);
        
        // check if a line contains vertex data and save in &v
        bool get_vertex(const char *buffer, vertex &v);
        
        // check if a line contains normal data and save in &n
        bool get_normal(const char *buffer, normal &n);
        
        // check if a line contains texco data and save in &t
        bool get_texco(const char *buffer, texco &t);
        
        // check if a line contains face data and save in &f
        bool get_face(const char *buffer, wfobj_face &f);
        
        // load a wav file
        wav *load_wav_file(const std::string &filename);
        
        // clear the data for a wav 
        void dispose_wav(wav *wav_data);
    }
}



#endif
