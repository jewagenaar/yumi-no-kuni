#include "primitives.h"
#include "kami.h"
#include "texman.h"
#include "fileutils.h"
#include "imgman.h"

using namespace kami;

texman::texman() {}

/* ===================== */
texman * texman::instance()
{
    static texman *__instance__ = new(std::nothrow) texman;
    return __instance__;
}

/* ==================================================================== */
GLuint texman::load_texture (const uint16_t &img_key, const uint16_t &key)
{
    bitmap *img = imgman::instance()->get_img(img_key);
    
    GLuint tex_id = load_texture(img, key);
    
    return tex_id;
}

/* ======================================================== */
GLuint texman::load_texture (bitmap *bmp, const uint16_t &key)
{
#ifdef __KAMI_DEBUG__
    assert(bmp);
#endif
    
    GLuint texture;
    
    glGenTextures (1, &texture);
    glBindTexture (GL_TEXTURE_2D, texture);
    
    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    glTexParameterf (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexEnvf       (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    
    if(bmp->alpha) {
        gluBuild2DMipmaps(GL_TEXTURE_2D, 4, bmp->img_width, bmp->img_height,
                          GL_RGBA, GL_UNSIGNED_BYTE, bmp->data);
    } else {
        gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp->img_width, bmp->img_height,
                          GL_RGB, GL_UNSIGNED_BYTE, bmp->data);
    }
    
    m_textures[key] = texture;
    return texture;
}

/* ======================================================================== */
GLuint texman::load_texture(const std::string &filename, const uint16_t &key)
{
    bitmap *img = fileutils::load_png(filename);
    
    GLuint tex_id = load_texture(img, key);
    
    delete img;
    
    return tex_id;
}

/* ========================================= */
GLuint texman::get_texture(const uint16_t &key)
{
    return m_textures[key];
}

/* ======================================== */
void texman::free_texture(const uint16_t &key)
{
    GLuint texture = m_textures[key];
    if (texture)
    {
        glDeleteTextures(1, &texture);
        m_textures.erase(key);
    }
}

/* ================= */
void texman::free_all()
{
    for(auto t_iter = m_textures.begin(); t_iter != m_textures.end(); ++t_iter)
    {
        GLuint texture = (*t_iter).second;
        glDeleteTextures(1, &texture);
    }
    m_textures.clear();
}
