//
//  modelman.cpp
//  kami
//
//  Created by John Evans Wagenaar on 2012/05/02.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include "../base/kami.h"
#include "modelman.h"
#include "../utils/fileutils.h"
#include "texman.h"
#include "shaderman.h"

using namespace kami;

modelman::modelman() {}

/* ======================== */
modelman *modelman::instance()
{
    static modelman * __instance = new modelman;
    return __instance;
}

/* =================================================================== */
void modelman::load_wfo(const std::string &filename, const uint16_t &key)
{
    wfobj *obj = fileutils::load_wfofile(filename.c_str());
    m_wfocache[key] = obj;
}

/* ====================================================== */
wfmodel *modelman::create_wfmodel(const metamodel &metadata)
{
    wfobj *obj = m_wfocache[metadata.data_key];
    
    GLuint tex_id    = texman::instance()->get_texture(metadata.texmap_key);
    GLuint bmap_id   = texman::instance()->get_texture(metadata.bmap_key);
    GLuint shader_id = shaderman::instance()->get_shader(metadata.shader_key);
    
    wfmodel *md = new wfmodel(obj);
    
    md->texid    = tex_id;
    md->bmapid   = bmap_id;
    md->shaderid = shader_id;
    
    m_wfmodels[metadata.key] = md;
    
    return md;
}

/* ============================================= */
wfmodel *modelman::get_wfmodel(const uint16_t &key)
{
    return m_wfmodels[key];
}

/* ======================================= */
wfobj *modelman::get_wfo(const uint16_t &key)
{
    return m_wfocache[key];
}

/* ============================================== */
void modelman::free_wfmodel(const uint16_t &key)
{
    wfmodel *model = m_wfmodels[key];
    m_wfmodels.erase(key);
    delete model;
}

/* ==================================================================== */
void modelman::load_md2(const std::string & filename, const uint16_t &tag)
{
    md2 *data  = fileutils::load_md2file(filename.c_str());
    m_md2cache[tag] = data;
}

/* ====================================== */
md2 * modelman::get_md2(const uint16_t &tag)
{
    return m_md2cache[tag];
}

/* ======================================================== */
md2model *modelman::create_md2model(const metamodel &metadata)
{
    md2      *data  = m_md2cache[metadata.data_key];
    md2model *model = new (std::nothrow) md2model(data);
    
    GLuint tex_id = texman::instance()->get_texture(metadata.texmap_key);
    model->texid  = tex_id;
    
    GLuint shader_id = shaderman::instance()->get_shader(metadata.shader_key);
    model->shaderid = shader_id;
    
    m_md2models[metadata.key] = model;
    
    return model;
}

/* ======================================================= */
md2model * modelman::get_md2model(const uint16_t &key)
{
    return m_md2models[key];
}

/* ============================================== */
void modelman::free_md2model(const uint16_t &key)
{
    md2model *model = m_md2models[key];
    m_md2models.erase(key);
    delete model;
}

/* =================== */
void modelman::free_all()
{
    std::unordered_map<uint16_t, wfmodel *>::iterator m_iter;
    for(m_iter = m_wfmodels.begin(); m_iter != m_wfmodels.end(); m_iter++)
    {
        wfmodel *model = m_iter->second;
        delete model;
    }
    m_wfmodels.clear();
    
    std::unordered_map<uint16_t, wfobj *>::iterator o_iter;
    for(o_iter = m_wfocache.begin(); o_iter != m_wfocache.end(); o_iter++)
    {
        wfobj *obj = o_iter->second;
        delete obj;
    }
    m_wfocache.clear();
}
