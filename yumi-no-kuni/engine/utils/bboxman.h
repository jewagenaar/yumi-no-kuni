//
//  bboxman.h
//  kami
//
//  Created by John Evans Wagenaar on 2012/05/16.
//  Copyright (c) 2012 John-Evans Wagenaar. All rights reserved.
//

#ifndef kami_bboxman_h
#define kami_bboxman_h

#import <unordered_map>
#import "../base/primitives.h"
#import "../base/kamimodel.h"
#import "../utils/fileutils.h"

namespace kami
{
    class bboxman
    {
        bboxman();
        std::unordered_map<uint16_t, kami::bbox *> m_boxes;
        
    public:
        // get an instance of the manager
        static bboxman *instance();
        
        //convert wfobj model to a bbox
        static bbox *convert(wfobj * obj);
        
        // create a bbox from wavefront object data
        bbox *create(wfobj *obj, const uint16_t &key);
        
        // get a specific bounding box
        bbox *get_bbox(const uint16_t &key);
        
        // create the bbox from the wavefront object data, and also translate by v
        bbox *create_and_translate(wfobj *obj, const vector &v);
        
        // free all memory
        void free_all();
    };
}

#endif
