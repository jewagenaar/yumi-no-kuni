#ifndef __yumi_no_kuni__imgman__
#define __yumi_no_kuni__imgman__

#include <map>
#include <iostream>
#include "kami.h"
#include "primitives.h"
#include "fileutils.h"

namespace kami
{
    class imgman
    {
        std::map<uint16_t, bitmap *> m_cache;
        
    public:
        static imgman *instance (void);
        
        bitmap* get_img  (const uint16_t &key);
        
        bitmap* load_png (const std::string &filename, const uint16_t &key);
        bitmap* load_bmp (const std::string &filename, const uint16_t &key);
        
        GLuint create_buffer (const uint16_t &key);
       
        void free_img (const uint16_t &key);
        void free_all (void);
        
    private:
         imgman (void);
        ~imgman (void);
    };
}

#endif /* defined(__yumi_no_kuni__imgman__) */
