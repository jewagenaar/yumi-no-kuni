//
//  glshaders.cpp
//  kami
//
//  Created by John Evans Wagenaar on 2012/03/18.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include <iostream>

#include "shaderman.h"
#include "../base/kami.h"
#include "../utils/fileutils.h"

using namespace kami;

shaderman *shaderman::instance()
{
    static shaderman * __instance = new shaderman;
    return __instance;
}

/* ====================================== */
GLuint shaderman::get_shader(const int &key)
{
    return m_shaders[key];
}

/* ================================================= */
GLuint shaderman::setup(const std::string &vert_shader,
                        const std::string &frag_shader,
                        const int &key)
{

#ifdef __KAMI_DEBUG__
    std::cout
    << "Setting up GLSL shader [" << key << "]"
    << " (" << vert_shader
    << ", " << frag_shader << ")"
    << std::endl;
#endif
    
    GLuint program = m_shaders[key];
    
    //only load if not already loaded
    if(!program)
    {
        std::string vs = fileutils::load_txt (vert_shader.c_str());
        std::string fs = fileutils::load_txt (frag_shader.c_str());
        
        GLuint vsp = glCreateShader (GL_VERTEX_SHADER);
        GLuint fsp = glCreateShader (GL_FRAGMENT_SHADER);
        
        GLuint program = glCreateProgram();
        
        glAttachShader (program, vsp);
        glAttachShader (program, fsp);
        
        const char * vs_char = vs.c_str();
        const char * fs_char = fs.c_str();
        
        glShaderSource (vsp, 1, &vs_char, NULL);
        glShaderSource (fsp, 1, &fs_char, NULL);
        
        glCompileShader (vsp);
        glCompileShader (fsp);
        
        glLinkProgram (program);
        glUseProgram  (program);
        
        
        errorlog_shader  (vsp);
        errorlog_shader  (fsp);
        errorlog_program (program);
        
        m_shaders[key] = program;
    }
    return program;
}

/* ======================================== */
void shaderman::errorlog_shader(GLuint shader)
{
    GLint result;
    GLint logLength;
    
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
    
    GLchar buffer[logLength];
    GLsizei length;
    
    glGetShaderInfoLog(shader, logLength, &length, buffer);
    glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
    std::cout << "Compile status of the shader " << result << std::endl;
    std::cout << buffer << std::endl;
}

/* ========================================== */
void shaderman::errorlog_program(GLuint program)
{
    GLint result;
    GLint logLength;
    
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
    
    GLchar buffer[logLength];
    GLsizei length;
    
    glGetProgramInfoLog(program, logLength, &length, buffer);
    glGetProgramiv(program, GL_VALIDATE_STATUS, &result);
    std::cout << "Validate status of the program " << result << std::endl;
    glGetProgramiv(program, GL_LINK_STATUS, &result);
    std::cout << "Link status of the program " << result << std::endl;
    glGetProgramiv(program, GL_ATTACHED_SHADERS, &result);
    std::cout << "Attached shaders of the program " << result << std::endl;
    std::cout << buffer << std::endl;
}
