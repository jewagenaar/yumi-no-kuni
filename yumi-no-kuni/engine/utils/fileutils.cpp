//
//  fileutils.cpp
//  kami
//
//  Created by John Evans Wagenaar on 2012/03/17.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include "../base/kami.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdint.h>
#include "fileutils.h"

using namespace kami;

/* ====================================================== */
std::string fileutils::load_txt(const std::string &filename)
{
    std::ifstream     fin(filename.c_str(), std::ios::in);
    std::stringstream buffer;
    
    if(fin.is_open()) {
        buffer << fin.rdbuf();
        fin.close();
    }
    
    return buffer.str();
}

/* ====================================================== */
bitmap *fileutils::load_bmp(const std::string &filename)
{
    bitmap *bmp = NULL;
    
    //open the file in binary mode
    std::ifstream fin(filename.c_str(), std::ios::binary);
    
    //check if file could be opened and proceed
    if(fin.is_open()) {
        bmp = new bitmap;
        fin.read(bmp->tag, 2);
        
        fin.read(reinterpret_cast<char *>(&bmp->filesize), 4);
        fin.seekg(10, std::ios_base::beg); //now we move to offset 10
        fin.read(reinterpret_cast<char *>(&bmp->offset_to_pixels), 4);
        fin.read(reinterpret_cast<char *>(&bmp->header_size), 4); //must be 40
        fin.read(reinterpret_cast<char *>(&bmp->img_width), 4);
        fin.read(reinterpret_cast<char *>(&bmp->img_height), 4);
        fin.read(reinterpret_cast<char *>(&bmp->planes), 2); //should be one
        fin.read(reinterpret_cast<char *>(&bmp->bpp), 2); //get bits per pixel
        fin.seekg(34, std::ios_base::beg); //jump ahead to find size of data
        fin.read(reinterpret_cast<char *>(&bmp->size), 4);
        fin.seekg(bmp->offset_to_pixels, std::ios_base::beg); //jump to pixel data
        
        fin.read(reinterpret_cast<char *>(&bmp->data), bmp->size);
        
        fin.close();
    }
    return bmp;
}

/* =================================================== */
wfobj * fileutils::load_wfofile(const char *filename)
{
    
    std::ifstream fin(filename);
    char buffer[BUFFER_SIZE];
    
#ifdef __KAMI_DEBUG__
    if(!fin.is_open()) {
        std::cout << "Could not open Wavefront file: " << filename << std::endl;
        assert(fin.is_open());
    }
#endif
    
    wfobj      *result = new wfobj;
    vertex     v;
    normal     n;
    texco      t;
    wfobj_face face;
    
    //read in the file
    while(!fin.eof()) {
        
        //fetch a line from the file into the buffer
        fin.getline(buffer, BUFFER_SIZE);
        //match the line against a template and
        //save it in the wfobj if it matches
        if(get_vertex(buffer, v)) {
            result->vertices.push_back(v);
        } else if (get_normal(buffer, n)) {
            result->normals.push_back(n);
        } else if (get_texco(buffer, t)) {
            result->texcos.push_back(t);
        } else if (get_face(buffer, face)) {
            result->faces.push_back(face);
        }
    }
    
    fin.close();
    
    return result;
}

/* ======================================================= */
bool fileutils::get_vertex(const char *buffer, vertex &v)
{
    if(sscanf(buffer, VERT_TEMPL, &v.x, &v.y, &v.z) == 3) {
        return true;
    }
    
    return false;
}

/* ======================================================= */
bool fileutils::get_normal(const char *buffer, normal &n)
{
    if(sscanf(buffer, NORM_TEMPL, &n.x, &n.y, &n.z) == 3) {
        return true;
    }
    
    return false;
}

/* ===================================================== */
bool fileutils::get_texco(const char *buffer, texco &t)
{
    if(sscanf(buffer, TXCO_TEMPL, &t.x, &t.y) == 2) {
        return true;
    }
    
    return false;
}

/* ======================================================== */
bool fileutils::get_face(const char *buffer, wfobj_face &f)
{
    if(sscanf(buffer, FACE_TEMPL_3VPF,
              &f.face_3vpf[0], &f.face_3vpf[1], &f.face_3vpf[2],
              &f.face_3vpf[3], &f.face_3vpf[4], &f.face_3vpf[5],
              &f.face_3vpf[6], &f.face_3vpf[7], &f.face_3vpf[8]) == 9) {
        return true;
    }
    
    return false;
}

/* ========================================================= */
wav *fileutils::load_wav_file(const std::string &filename)
{
    std::ifstream fin(filename, std::ios::binary | std::ios::in);
    
#ifdef __KAMI_DEBUG__
    if(!fin.is_open()) {
        std::cout << "Could not open WAV file: " << filename << std::endl;
        assert(fin.is_open());
    }
#endif
    
    wav *result = new wav;
    
    fin.read(result->tag, 4);
    READ_BYTES(fin, result->chunk_size, 4);
    READ_BYTES(fin, result->format,     4);
    
    fin.read(result->fmt_tag, 4);
    READ_BYTES(fin, result->fmt_size,        4);
    READ_BYTES(fin, result->audio_format,    2);
    READ_BYTES(fin, result->num_channels,    2);
    READ_BYTES(fin, result->sample_rate,     4);
    READ_BYTES(fin, result->byte_rate,       4);
    READ_BYTES(fin, result->block_align,     2);
    READ_BYTES(fin, result->bits_per_sample, 2);
    
    fin.read(result->data_tag, 4);
    READ_BYTES(fin, result->data_size, 4);
    
    result->data = new byte[result->data_size];
    
    fin.read(reinterpret_cast<char *>(result->data), result->data_size);
    
    fin.close();
    
    return result;
}

/* ===================================== */
void fileutils::dispose_wav(wav *wav_data)
{
    delete wav_data->data;
    wav_data = 0;
    
    delete wav_data;
}

/* ================================================= */
md2 * fileutils::load_md2file(const char *filename)
{
    // TODO: this still uses C code, need to make it C++
    
    md2 * mdl = new md2;
	FILE * fp;
	int i;
	fp = fopen(filename, "rb");
    
	if(!fp) {
		fprintf(stderr, "Error: couldn't open \"%s\"!\n", filename);
		return 0;
	}
	
    /* Read header */
	fread(&mdl->header, 1, sizeof(md2_header), fp);
	if((mdl->header.ident != 844121161) || (mdl->header.version != 8)) {
		/* Error! */
		fprintf (stderr, "Error: bad version or identifier\n");
		fclose (fp);
		return 0;
	}
    
	/* Memory allocations */
	mdl->skins       = new md2_skin     [mdl->header.num_skins];
	mdl->texcoords   = new md2_texcoord [mdl->header.num_st];
	mdl->triangles   = new md2_triangle [mdl->header.num_tris];
	mdl->frames      = new md2_frame    [mdl->header.num_frames];
	mdl->gl_commands = new int          [mdl->header.num_glcmds];
	
    /* Read model data */
	fseek(fp, mdl->header.offset_skins, SEEK_SET);
	fread(mdl->skins, sizeof(md2_skin), mdl->header.num_skins, fp);
	fseek(fp, mdl->header.offset_st, SEEK_SET);
	fread(mdl->texcoords, sizeof(md2_texcoord), mdl->header.num_st, fp);
	fseek(fp, mdl->header.offset_tris, SEEK_SET);
	fread(mdl->triangles, sizeof(md2_triangle), mdl->header.num_tris, fp);
	fseek(fp, mdl->header.offset_glcmds, SEEK_SET);
	fread(mdl->gl_commands, sizeof(int), mdl->header.num_glcmds, fp);
    
	/* Read frames */
	fseek(fp, mdl->header.offset_frames, SEEK_SET);
	for (i = 0; i < mdl->header.num_frames; ++i) {
		/* Memory allocation for vertices of this frame */
		mdl->frames[i].verts = new md2_vertex[mdl->header.num_vertices];
        
		/* Read frame data */
		fread(mdl->frames[i].scale,     sizeof(md2_vector), 1,  fp);
		fread(mdl->frames[i].translate, sizeof(md2_vector), 1,  fp);
		fread(mdl->frames[i].name,      sizeof(char),       16, fp);
		fread(mdl->frames[i].verts,     sizeof(md2_vertex), mdl->header.num_vertices, fp);
	}
	fclose (fp);
    
	return mdl;
}

/* ====================================================== */
bitmap * fileutils::load_png(const std::string &filename)
{
    bitmap * bmp = NULL;
    std::ifstream fin(filename.c_str(), std::ios::binary);
    
    if(fin.is_open()) {
        
        bmp = new bitmap;
        
        //create a png read structure - set default error handling
        png_structp struct_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
        
        //create a png info structure
        png_infop info_ptr = png_create_info_struct(struct_ptr);
        
        png_set_read_fn(struct_ptr, static_cast<png_voidp>(&fin), read_png_data);
        //set how many bytes wehave read of the info structure
        //read in the file info
        png_read_info(struct_ptr, info_ptr);
        
        // get image info
        png_uint_32 img_width      = png_get_image_width (struct_ptr, info_ptr);
        png_uint_32 img_height     = png_get_image_height(struct_ptr, info_ptr);
        png_uint_32 img_bpp        = png_get_bit_depth   (struct_ptr, info_ptr);
        png_uint_32 img_channels   = png_get_channels    (struct_ptr, info_ptr);
        png_uint_32 img_color_type = png_get_color_type  (struct_ptr, info_ptr);
        
        // if transparency is set add an alpha channel
        if (img_color_type == PNG_COLOR_TYPE_RGBA) {
            bmp->alpha = 1;
        } else {
            bmp->alpha = 0;
        };
        
        png_bytep         *row_ptr  = new png_bytep[img_height];
        const png_uint_32  row_len  = img_width * img_bpp * img_channels / 8;
        const unsigned int data_len = img_height * row_len;
        
        byte *img_data = new byte[data_len];
        
        // now point each row_ptr to a location in img_data
        // OpenGL expects it to be in this format:
        for (size_t i = 0; i < img_height; i++) {
            unsigned long q = (img_height - i - 1) * row_len;
            row_ptr[i]      = static_cast<png_bytep>(img_data + q);
        }
        
        png_read_image(struct_ptr, row_ptr);
        
        bmp->img_height = img_height;
        bmp->img_width  = img_width;
        bmp->bpp        = img_bpp;
        bmp->data       = img_data;
        
        fin.close();
    }
    
    ELSE_CERR("Could not open PNG file: ", filename)
    
    return bmp;
}

/* ==================================================================== */
void fileutils::read_png_data(png_structp struct_ptr, png_bytep data, png_size_t len)
{
    png_voidp in      = png_get_io_ptr(struct_ptr);
    std::ifstream *is = static_cast<std::ifstream *>(in);
    is->read(reinterpret_cast<char *>(data), len);
}


