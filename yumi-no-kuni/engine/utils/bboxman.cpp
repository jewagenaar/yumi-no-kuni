//
//  bboxman.cpp
//  kami
//
//  Created by John Evans Wagenaar on 2012/05/16.
//  Copyright (c) 2012 John-Evans Wagenaar. All rights reserved.
//

#include "bboxman.h"
#include "../base/kamimath.h"
#include "../utils/fileutils.h"

using namespace kami;

bboxman::bboxman() {}

/* ====================== */
bboxman *bboxman::instance()
{
    static bboxman * __instance = new bboxman;
    return __instance;
}

/* ========================================= */
bbox *bboxman::convert(wfobj *obj)
{
    bbox * result = new bbox;
    result->num_tris  = static_cast<uint32_t>(obj->faces.size());
    result->triangles = (coltri *) malloc(sizeof(coltri) * result->num_tris);
    
    std::vector<wfobj_face>::iterator iter;
    
    int i = 0;
    
    // For each face create a collision coltri
    for(iter = obj->faces.begin(); iter < obj->faces.end(); iter++) {
        
        wfobj_face face = *iter;
        
        short vertex_index_a = face.face_3vpf[0] - 1;
        short vertex_index_b = face.face_3vpf[3] - 1;
        short vertex_index_c = face.face_3vpf[6] - 1;
        
        short normal_index = face.face_3vpf[2] - 1;
        
        coltri t;
        
        t.vertex_a = obj->vertices[vertex_index_a];
        t.vertex_b = obj->vertices[vertex_index_b];
        t.vertex_c = obj->vertices[vertex_index_c];
        
        t.n = obj->normals[normal_index];
        
        t.vector_p.x = t.vertex_a.x - t.vertex_b.x;
        t.vector_p.y = t.vertex_a.y - t.vertex_b.y;
        t.vector_p.z = t.vertex_a.z - t.vertex_b.z;
        
        t.vector_q.x = t.vertex_b.x - t.vertex_c.x;
        t.vector_q.y = t.vertex_b.y - t.vertex_c.y;
        t.vector_q.z = t.vertex_b.z - t.vertex_c.z;
        
        t.vector_r.x = t.vertex_c.x - t.vertex_a.x;
        t.vector_r.y = t.vertex_c.y - t.vertex_a.y;
        t.vector_r.z = t.vertex_c.z - t.vertex_a.z;
        
        t.edge_a.start = t.vertex_b;
        t.edge_a.end   = t.vertex_a;
        t.edge_a.dir   = t.vector_p;
        
        t.edge_b.start = t.vertex_c;
        t.edge_b.end   = t.vertex_b;
        t.edge_b.dir   = t.vector_q;
        
        t.edge_c.start = t.vertex_a;
        t.edge_c.end   = t.vertex_c;
        t.edge_c.dir   = t.vector_r;
        
        t.color.r = 0.0;
        t.color.g = 0.0;
        t.color.b = 1.0;
        t.color.a = 0.9;
        
        result->triangles[i++] = t;
    }
    return result;
}

/* ====================================== */
bbox *bboxman::get_bbox(const uint16_t &key)
{
    return m_boxes[key];
}

/* =========================================================== */
bbox *bboxman::create_and_translate(wfobj *obj, const vector &v)
{
    
    bbox *result = bboxman::convert(obj);
    
    for(int i = 0; i < result->num_tris; i++) {
        
        coltri * tri = result->triangles + i;
        vmath::inline_translate(&tri->vertex_a, v);
        vmath::inline_translate(&tri->vertex_b, v);
        vmath::inline_translate(&tri->vertex_c, v);
    }
    
    return result;
}

/* ================================================ */
bbox *bboxman::create(wfobj *obj, const uint16_t &key)
{
    bbox *result = bboxman::convert(obj);
    m_boxes[key] = result;
    
    return result;
}

/* ================== */
void bboxman::free_all()
{
    std::unordered_map<uint16_t, bbox *>::iterator iter;
    for(iter = m_boxes.begin(); iter != m_boxes.end(); iter++) {
        bbox *item = iter->second;
        delete item;
    }
    m_boxes.clear();
}
