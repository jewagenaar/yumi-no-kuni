//
//  modelman.h
//  kami
//
//  Created by John Evans Wagenaar on 2012/05/02.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef kami_modelman_h
#define kami_modelman_h

#include <vector>
#include <unordered_map>
#include "../base/kamimodel.h"
#include "../base/kami.h"
#include "wfmodeldata.h"
#include "md2model.h"

namespace kami {
    // model types that can be loaded
    enum KAMI_MODEL_TYPE {
        MODEL_WFO,
        MODEL_MD2
    };
    
    // holds the keys for all the data associated with a renderable model
    typedef struct t_metamodel {
        char     tag[8];     // optional null terminated tag name
        uint8_t  type;       // the type of model (MODEL_TYPE)
        uint16_t key;        // the key for the complete model
        uint16_t data_key;   // the key for the modeldata
        uint16_t texmap_key; // the key for the texture map
        uint16_t bmap_key;   // the key for the bump map
        uint16_t shader_key; // the key for the shader program
    } metamodel;

    class modelman
    {
        modelman();
        
        std::unordered_map<uint16_t, kami::wfobj *> m_wfocache;
        std::unordered_map<uint16_t, kami::md2 *>   m_md2cache;
        
        std::unordered_map<uint16_t, wfmodel *>  m_wfmodels;
        std::unordered_map<uint16_t, md2model *> m_md2models;
        
    public:
        static modelman *instance();
        
        // load a WFO file, store in cache with tag
        void load_wfo (const std::string &filename, const uint16_t &key);
        // load a MD2 file, store in cache with tag
        void load_md2 (const std::string &filename, const uint16_t &key);
        
        // retrieve loaded WF obj from cache
        wfobj *get_wfo (const uint16_t &key);
        // retrieve a MD2 file from the cache
        md2   *get_md2 (const uint16_t &key);
        
        // create a renderable instance, store in cache with tag
        wfmodel  *create_wfmodel  (const metamodel &metadata);
        // create a renderable MD2 instance, store in cache with tag
        md2model *create_md2model (const metamodel &metadata);
        
        // retrieve a renderable instance from the cache
        wfmodel  *get_wfmodel  (const uint16_t &key);
        // retrieve a renderable MD2 instance from the cache
        md2model *get_md2model (const uint16_t &key);
        
        //free all memory
        void free_all();
        //free memory for the wavefront model
        void free_wfmodel  (const uint16_t &key);
        //free memory for the MD2 model data
        void free_md2model (const uint16_t &tag);
    };
}



#endif
