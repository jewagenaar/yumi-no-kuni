#include "imgman.h"

using namespace kami;

imgman *imgman::instance ()
{
    static imgman *_instance = new (std::nothrow) imgman;
    
    return _instance;
}

/* =========== */
imgman::imgman ()
{
    
}

/* ============ */
imgman::~imgman ()
{
    
}

/* ======================================== */
bitmap* imgman::get_img  (const uint16_t &key)
{
    return m_cache[key];
}

/* ===================================================================== */
bitmap* imgman::load_png (const std::string &filename, const uint16_t &key)
{
    bitmap *bmp = fileutils::load_png(filename.c_str());

    m_cache[key] = bmp;
    
    return bmp;
}

/* ===================================================================== */
bitmap* imgman::load_bmp (const std::string &filename, const uint16_t &key)
{
    bitmap *bmp = fileutils::load_bmp(filename.c_str());

    m_cache[key] = bmp;
    
    return bmp;
}

/* ===================================== */
void imgman::free_img (const uint16_t &key)
{
    bitmap *bmp = m_cache[key];
    
    if (bmp) {
        delete bmp;
        m_cache.erase(key);
    }
}

/* ====================== */
void imgman::free_all (void)
{
    for (auto t_iter = m_cache.begin(); t_iter != m_cache.end(); ++t_iter) {
        bitmap *bmp = (*t_iter).second;
        delete bmp;
    }
    m_cache.clear();
}

/* ============================================ */
GLuint imgman::create_buffer (const uint16_t &key)
{
    GLuint bufferIndex = 0;

    bitmap *img = m_cache[key];
    
    glGenBuffers(1, &bufferIndex);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER,
                 bufferIndex);
    glBufferData(GL_PIXEL_UNPACK_BUFFER,
                 4 * img->img_width * img->img_height, img->data,
                 GL_STATIC_DRAW);
    
    
    return bufferIndex;
}


