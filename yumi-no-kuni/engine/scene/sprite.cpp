#include "sprite.h"

using namespace kami;

/* ========== */
sprite::sprite()
{
}

/* =============== */
void sprite::render()
{
    glPushMatrix();
    glLoadIdentity();
    
    glColor4f(bgd_color.r, bgd_color.g, bgd_color.b, bgd_color.a);
    
    if(texid) {
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, texid);
    }
    
    glBegin(GL_TRIANGLE_STRIP);
    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(pos.x, pos.y, pos.z);
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(pos.x, pos.y - height, pos.z);
    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(pos.x + width, pos.y, pos.z);
    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(pos.x + width, pos.y - height, pos.z);
    glEnd();
    
    if(texid) {
        glBindTexture(GL_TEXTURE_2D, 0);
        glDisable(GL_TEXTURE_2D);
    }
    
    glPopMatrix();
}

