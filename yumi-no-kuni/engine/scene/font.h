//
//  font.h
//  yumi-no-kuni
//
//  Created by John-Evans Wagenaar on 29/04/2013.
//  Copyright (c) 2013 John-Evans Wagenaar. All rights reserved.
//

#ifndef __yumi_no_kuni__font__
#define __yumi_no_kuni__font__

#include "../base/primitives.h"
#include "../base/kami.h"

#define KAMI_FONT_SML 0.01f
#define KAMI_FONT_MED 0.05f
#define KAMI_FONT_LRG 0.10f

namespace kami
{
    class font
    {
        friend class TextOut;
        
        GLuint m_texid;
    public:
        static font *debug();
        
        font(const GLuint &texid);
        
        void render(const char      &c,
                    const float     &size,
                    const rgb_color &color);
        
    };
}



#endif /* defined(__yumi_no_kuni__font__) */
