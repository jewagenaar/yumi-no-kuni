//
//  hudlayer.h
//  yumi-no-kuni
//
//  Created by John-Evans Wagenaar on 26/04/2013.
//  Copyright (c) 2013 John-Evans Wagenaar. All rights reserved.
//

#ifndef __yumi_no_kuni__hudlayer__
#define __yumi_no_kuni__hudlayer__

#include <vector>

#include "kami.h"
#include "sprite.h"
#include "textout.h"

namespace kami
{
    // hud layer
    class hudlayer
    {
        std::vector<sprite *> m_sprites;
        
        std::string  m_debugmsg [20];
        int          m_lastindex = 0;
    public:
        textout *debugout = 0;
        
        hudlayer (void);
        
        void render (void);
        void add    (sprite *sprite);
        void clear  (void);
        void debug  (const int &line, const std::string &msg);

        virtual void render_text(void);
    };
}


#endif /* defined(__yumi_no_kuni__hudlayer__) */
