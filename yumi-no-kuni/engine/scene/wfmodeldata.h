//
//  modeldata.h
//  kami
//
//  Created by John Evans Wagenaar on 2012/03/18.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef kami_modeldata_h
#define kami_modeldata_h

#include <vector>
#include "renderable.h"
#include "../base/kamimodel.h"
#include "../base/kami.h"

namespace kami
{
    class wfmodel : public renderable
    {
        wfobj *m_data;
        
    public:
        GLuint texid;
        GLuint bmapid;
        GLuint shaderid;
        
        point  lightpos;
        point  position;
        
        wfmodel(wfobj *data);

        void render();
    };
}


#endif
