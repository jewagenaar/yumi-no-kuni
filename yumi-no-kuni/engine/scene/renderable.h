#ifndef kami_renderable_h
#define kami_renderable_h

#include "kami.h"
#include "animation.h"

namespace kami
{
    class renderable
    {
    public:
        virtual void render() = 0;
    };
    
    class null_renderable : public renderable
    {
        void render () { }
    };
}



#endif
