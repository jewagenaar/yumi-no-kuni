//
//  textout.cpp
//  yumi-no-kuni
//
//  Created by John-Evans Wagenaar on 29/04/2013.
//  Copyright (c) 2013 John-Evans Wagenaar. All rights reserved.
//

#include "textout.h"

using namespace kami;

textout::textout():
fontstyle (0),
width     (10),
size      (KAMI_FONT_MED),
position  ({ 0.0f, 0.0f, 0.0f }),
color     ({ 1.0f, 1.0f, 1.0f, 1.0f })
{

}

/* ================================ */
void textout::render(const char *text)
{
    char c;
    int  i = 0; // char index in text
    int cr = 0; // carraige return

    glPushMatrix();
    glLoadIdentity();
    
    // position the text on screen
    glTranslatef(position.x, position.y, position.z);
    
    // now draw all the characters
    while((c = text[i++]) != 0) {
    
        // if we reach the end of line or new line
        if(c == '\n' || cr == width - 1) {
            glTranslatef(-size*cr, -size, 0.0f);
            cr = 0;
            
            // if its a newline or space we are done
            if(c == '\n' || c == ' ') {
                continue;
            }
        }
        
        // move along and render
        glTranslatef(size, 0.0f, 0.0f);
        fontstyle->render(c, size, color);
        
        // update the indices
        cr++;
    }
    glPopMatrix();
}