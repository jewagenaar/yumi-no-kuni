#ifndef __kami__md2modeldata__
#define __kami__md2modeldata__

#include <iostream>
#include "animation.h"
#include "kamimodel.h"

namespace kami
{
    class md2model 
    {
        md2 *m_data;

    public:
        GLuint texid;
        GLuint shaderid;
        point  position;
        vector adjust; 
        
        md2model    (md2 *data);
        void render (const anim_state &st);
    };
}



#endif /* defined(__kami__md2modeldata__) */
