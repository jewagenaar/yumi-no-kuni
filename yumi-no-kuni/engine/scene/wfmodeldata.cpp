//
//  modeldata.cpp
//  kami
//
//  Created by John Evans Wagenaar on 2012/03/18.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include "wfmodeldata.h"
#include "../base/primitives.h"
#include "../base/kamimodel.h"
#include "../base/kamimath.h"
#include <vector>
#include <iostream>

using namespace kami;

wfmodel::wfmodel(wfobj *data):
m_data   (data),
texid    (0),
bmapid   (0),
shaderid (0)
{
    INIT_VOP(lightpos, 0.0f);
    INIT_VOP(position, 0.0f);
}

/* ==================== */
void wfmodel::render()
{
    glUseProgram(shaderid);
    
    // setup the texture map parameters
    if(texid) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texid);
        
        int u_TextureSampler = glGetUniformLocation(shaderid, "u_TextureSampler");
        glUniform1i(u_TextureSampler, 0);
    }
    
    // setup the bump map parameters
    if(bmapid) {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, bmapid);
        
        int u_BumpMapSampler = glGetUniformLocation(shaderid, "u_BumpMapSampler");
        glUniform1i(u_BumpMapSampler, 1);
    }
    
    
    // now setup the global light source
    int u_LightPos = glGetUniformLocation(shaderid, "u_LightPos");
    glUniform4f(u_LightPos, lightpos.x, lightpos.y, lightpos.z, 1.0f);
    
    glEnable(GL_DEPTH_TEST);
    
    kami::polyface pface;
    short vertex_index_a, vertex_index_b, vertex_index_c;
    short texco_index_a,  texco_index_b,  texco_index_c;
    short normal_index_a, normal_index_b, normal_index_c;
    
    glPushMatrix();
    
    glTranslatef(position.x, position.y, position.z);
    
    glBegin(GL_TRIANGLES);
    for(auto iter = m_data->faces.begin(); iter < m_data->faces.end(); iter++) {
        kami:kami::wfobj_face face = *iter;
        
        vertex_index_a = face.face_3vpf[0] - 1;
        vertex_index_b = face.face_3vpf[3] - 1;
        vertex_index_c = face.face_3vpf[6] - 1;
        
        texco_index_a = face.face_3vpf[1] - 1;
        texco_index_b = face.face_3vpf[4] - 1;
        texco_index_c = face.face_3vpf[7] - 1;
        
        normal_index_a = face.face_3vpf[2] - 1;
        normal_index_b = face.face_3vpf[5] - 1;
        normal_index_c = face.face_3vpf[8] - 1;
        
        pface.va = m_data->vertices[vertex_index_a];
        pface.vb = m_data->vertices[vertex_index_b];
        pface.vc = m_data->vertices[vertex_index_c];
        
        pface.na = m_data->normals[normal_index_a];
        pface.nb = m_data->normals[normal_index_b];
        pface.nc = m_data->normals[normal_index_c];
        
        pface.ta = m_data->texcos[texco_index_a];
        pface.tb = m_data->texcos[texco_index_b];
        pface.tc = m_data->texcos[texco_index_c];
        
        glNormal3f(pface.na.x, pface.na.y, pface.na.z);
        glTexCoord2f(pface.ta.x, pface.ta.y);
        glVertex3f(pface.va.x, pface.va.y, pface.va.z);
      
        glNormal3f(pface.nb.x, pface.nb.y, pface.nb.z);
        glTexCoord2f(pface.tb.x, pface.tb.y);
        glVertex3f(pface.vb.x, pface.vb.y, pface.vb.z);
        
        glNormal3f(pface.nc.x, pface.nc.y, pface.nc.z);
        glTexCoord2f(pface.tc.x, pface.tc.y);
        glVertex3f(pface.vc.x, pface.vc.y, pface.vc.z);
    }
    glEnd();
    
    glPopMatrix();
    
    if(bmapid) {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    
    if(texid) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}
