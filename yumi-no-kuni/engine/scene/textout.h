//
//  textout.h
//  yumi-no-kuni
//
//  Created by John-Evans Wagenaar on 29/04/2013.
//  Copyright (c) 2013 John-Evans Wagenaar. All rights reserved.
//

#ifndef __yumi_no_kuni__textout__
#define __yumi_no_kuni__textout__

#include <iostream>

#include "../base/kami.h"
#include "font.h"

namespace kami
{
    class textout
    {
    public:
        font      *fontstyle;
        int        width;
        float      size;
        point      position;
        rgb_color  color;
        
        textout();
        
        void render(const char *text);
    };
}



#endif /* defined(__yumi_no_kuni__textout__) */
