//
//  font.cpp
//  yumi-no-kuni
//
//  Created by John-Evans Wagenaar on 29/04/2013.
//  Copyright (c) 2013 John-Evans Wagenaar. All rights reserved.
//

#include "font.h"
#include "texman.h"

using namespace kami;

font *font::debug()
{
    static uint16_t texId = texman::instance()->get_texture(KAMI_DEBUG_FONT);
    static font *__font   = new font(texId);
    
    return __font;
}

/* ========================= */
font::font(const GLuint &texid):
m_texid (texid)
{
}

/* =========================================== */
void font::render(const char      &c,
                  const float     &size,
                  const rgb_color &color)
{
    static float W = 0.0625f;
    static float H = 0.0625f;
    
    float w_0 = W * (c % 0x10);
    float w_1 = w_0 + W;
    
    float h_0 = 1.0f - H * (c >> 4);
    float h_1 = h_0 - H;
    
    glColor4f(color.r, color.g, color.b, color.a);
    
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, m_texid);
    
    glBegin(GL_TRIANGLE_STRIP);
    
    glTexCoord2f(w_0,  h_0);
    glVertex3f  (0.0f, size, -1.0f);
    
    glTexCoord2f(w_0,  h_1);
    glVertex3f  (0.0f, 0.0f, -1.0f);
    
    glTexCoord2f(w_1,  h_0);
    glVertex3f  (size, size, -1.0f);
    
    glTexCoord2f(w_1,  h_1);
    glVertex3f  (size, 0.0f, -1.0f);
    
    glEnd();
    
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
}