//
//  md2model.cpp
//  kami
//
//  Created by John-Evans Wagenaar on 2012/07/29.
//  Copyright (c) 2012 John-Evans Wagenaar. All rights reserved.
//

#include "md2model.h"
#include "normals.h"

using namespace kami;

/* ======================= */
md2model::md2model(md2 *data) :
m_data        (data),
texid         (0),
position      (NULL_VEC),
adjust        (NULL_VEC)
{
}

/* ===================================== */
void md2model::render(const anim_state &st)
{
	md2_frame  *pframe1, *pframe2;
	md2_vertex *pvert1,  *pvert2;
	md2_vector  v_curr,   v_next, v;
    
	GLfloat s, t;
    
    glUseProgram(shaderid);
    
    GLint loc = glGetUniformLocation(shaderid, "objPos");
    glUniform4f(loc, position.x, position.y, position.z, 0.0);
    
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    if(texid) {
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, texid);
    }

	/* Draw the model */
	glPushMatrix();
	glRotatef(90.0, 0.0, 0.0, 1.0);
    glTranslatef(adjust.x, adjust.y, adjust.z);
    glScalef(0.05, 0.05, 0.05);
    
	glBegin (GL_TRIANGLES);
	for(int i = 0; i < m_data->header.num_tris; ++i) {
		/* Draw each vertex */
		for(int j = 0; j < 3; ++j) {
			pframe1 = &m_data->frames[st.cfi];
			pframe2 = &m_data->frames[st.cfi + 1];
            
			pvert1 = &pframe1->verts[m_data->triangles[i].vertex[j]];
			pvert2 = &pframe2->verts[m_data->triangles[i].vertex[j]];
            
			v_curr[0] = (pframe1->scale[0] * pvert1->v[0]) + pframe1->translate[0];
			v_curr[1] = (pframe1->scale[1] * pvert1->v[1]) + pframe1->translate[1];
			v_curr[2] = (pframe1->scale[2] * pvert1->v[2]) + pframe1->translate[2];
            
			v_next[0] = (pframe2->scale[0] * pvert2->v[0]) + pframe2->translate[0];
			v_next[1] = (pframe2->scale[1] * pvert2->v[1]) + pframe2->translate[1];
			v_next[2] = (pframe2->scale[2] * pvert2->v[2]) + pframe2->translate[2];
            
			s = (GLfloat) m_data->texcoords[m_data->triangles[i].st[j]].s/m_data->header.skinwidth;
			t = (GLfloat) m_data->texcoords[m_data->triangles[i].st[j]].t/m_data->header.skinheight;
			
            /* Pass texture coordinates to OpenGL */
			glTexCoord2f(s, t);
            
			/* Normal vector */
			glNormal3fv(normals_table[(int) pvert1->normalIndex]);
            
			v[0] = v_curr[0] + st.ipf * (v_next[0] - v_curr[0]);
			v[1] = v_curr[1] + st.ipf * (v_next[1] - v_curr[1]);
			v[2] = v_curr[2] + st.ipf * (v_next[2] - v_curr[2]);
			glVertex3fv(v);
		}
	}
	glEnd();
    
    if(texid) {
        glBindTexture(GL_TEXTURE_2D, 0);
        glDisable(GL_TEXTURE_2D);
    }
    
    glEnable(GL_CULL_FACE);
	glPopMatrix();
}

