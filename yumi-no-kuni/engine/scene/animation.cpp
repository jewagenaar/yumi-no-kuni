//
//  animation.cpp
//  kami
//
//  Created by John-Evans Wagenaar on 2012/08/01.
//  Copyright (c) 2012 John-Evans Wagenaar. All rights reserved.
//

#include "animation.h"

using namespace kami;

animation::animation(const int & start, const int & frameCount, const float & ipc) :
si  (start),
fc  (frameCount),
ipc (ipc)
{
    static uint32_t __ID = 0;
    ID = __ID++;
}

/* =============================================================================== */
anim_oneshot::anim_oneshot(const int &start, const int &framecount, const float &ipc) :
animation(start, framecount, ipc)
{
}

/* ======================= */
anim_oneshot::~anim_oneshot()
{
    
}

/* ====================================== */
void anim_oneshot::step(anim_state &s) const
{
    if (s.fin) {
        return;
    }
    
    s.ipf += ipc;
    
    if (s.ipf > 1.0) {
        s.cfi = s.cfi + 1;
        s.ipf = 0.0f;
    }
    
    if (s.cfi > si + fc) {
        s.fin = true;
    }
}

/* ========================================================================= */
anim_loop::anim_loop(const int &start, const int &framecount, const float &ipc) :
animation(start, framecount, ipc)
{
}

/* ================= */
anim_loop::~anim_loop()
{
}

/* =================================== */
void anim_loop::step(anim_state &s) const
{
    s.ipf += ipc;
    
    if(s.ipf > 1.0) {
        s.cfi = s.cfi + 1;
        s.ipf = 0.0f;
    }
    
    if(s.cfi >= si + fc) {
        s.cfi = si;
    }
}

