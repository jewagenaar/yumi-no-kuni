#ifndef kami_animation_h
#define kami_animation_h

#include <stdint.h>

namespace kami
{
    class anim_state
    {
    public:
        float ipf = 0.0f;  // the interpolation factor
        short cfi = 0;     // the current frame index
        bool  fin = true;  // still playing?
        
        uint32_t anim_id; // the animation id
    };
    
    // virtual base class for animations
    class animation
    {
    public:
        short si;  // start index
        short fc;  // framecount
        float ipc; // interpolation constant
        
        uint32_t ID;
        
        animation(const int &start, const int &framecount, const float &ipc);
        virtual ~animation (void) {};
        short    current   (void);
        void     reset     (void);
        
        virtual void step (anim_state &s) const = 0;
    };
    
    // an animation that just plays once
    class anim_oneshot : public animation
    {
    public:
        anim_oneshot  (const int &start, const int &framecount, const float &ipc);
        ~anim_oneshot ();
        void step     (anim_state &s) const;
    };

    // a looping animation that jumps back to the start when done
    class anim_loop : public animation
    {
    public:
        anim_loop  (const int &start, const int &framecount, const float &ipc);
        ~anim_loop ();
        void step  (anim_state &s) const;
    };
        
    // null object animation
    class anim_null : public animation
    {
    public:
        anim_null  () : animation(0, 0, 0.0f) {}
        ~anim_null () {}
        void step  (anim_state &s) const {}
    };
}





#endif
