//
//  scene.h
//  kami
//
//  Created by John Evans Wagenaar on 2012/03/18.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef kami_scene_h
#define kami_scene_h

#include <vector>
#include <string>

#include "renderable.h"
#include "hudlayer.h"
#include "pdvobject.h"

#include "kami.h"
#include "effects.h"

namespace kami
{
    class scene : public renderable
    {
    public:
        renderable *r = new (std::nothrow) null_renderable;
        
        point    position;
        rotation rotation;
        
        scene  (void);
        ~scene (void);
        
        void render (void);
        
#ifdef __KAMI_DEBUG__
        std::vector<pdvobject> pdv_objects;
#endif
    };
}


#endif
