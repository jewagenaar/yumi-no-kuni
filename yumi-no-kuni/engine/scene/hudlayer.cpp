#include "hudlayer.h"
#include "imgman.h"

using namespace kami;

/* ============== */
hudlayer::hudlayer()
{
}

/* ================= */
void hudlayer::render()
{
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(-1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 100.0f);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    
    glUseProgram(0);
    
    // setup blending
    glEnable(GL_BLEND);
    glBlendEquation(GL_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // render all the sprites on the HUD
    for (auto iter = m_sprites.begin(); iter < m_sprites.end(); iter++) {
        sprite *spr = *iter;
        spr->render();
    }
    
    render_text();
    
    glPopMatrix();
    // and disable blending again
    glDisable(GL_BLEND);
    
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    
    glMatrixMode(GL_MODELVIEW);
}

/* ============================= */
void hudlayer::add (sprite *sprite)
{
    m_sprites.push_back(sprite);
}

/* ================ */
void hudlayer::clear()
{
    m_sprites.clear();
}

/* ======================================================= */
void hudlayer::debug(const int &line, const std::string &msg)
{
    m_debugmsg[line] = msg;
    m_lastindex = m_lastindex < line ? line : m_lastindex;
}

/* ====================== */
void hudlayer::render_text()
{
    if (debugout) {
        // build up a debug message
        std::string msg;
        for(int i = 0; i <= m_lastindex; i++) {
            msg.append(m_debugmsg[i] + "\n");
        }
        debugout->render(msg.c_str());
    }
}

