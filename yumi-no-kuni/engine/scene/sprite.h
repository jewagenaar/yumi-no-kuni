//
//  sprite.h
//  yumi-no-kuni
//
//  Created by John-Evans Wagenaar on 26/04/2013.
//  Copyright (c) 2013 John-Evans Wagenaar. All rights reserved.
//

#ifndef __yumi_no_kuni__sprite__
#define __yumi_no_kuni__sprite__

#include <iostream>
#include "renderable.h"
#include "primitives.h"

namespace kami
{
    class sprite : public renderable
    {
        
    public:
        float width;
        float height;
        
        point     pos       = { 0.0f, 0.0f, -1.0f };
        rgb_color bgd_color = { 1.0f, 1.0f, 1.0f, 1.0f };
        GLuint    texid;
        
        sprite (void);
        
        void render (void);
    };
}


#endif /* defined(__yumi_no_kuni__sprite__) */
