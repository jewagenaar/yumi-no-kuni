//
//  scene.cpp
//  kami
//
//  Created by John Evans Wagenaar on 2012/03/18.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include "renderable.h"
#include "scene.h"
#include "shaderman.h"

using namespace kami;

/* ============ */
scene::scene():
position (NULL_VEC),
rotation (NULL_VEC)
{
}

/* ============= */
scene::~scene() { }

/* ============== */
void scene::render()
{
    // clear out the previous frame
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    GLfloat fogColor[4] = {0.0, 0.7, 0.0, 0.4};
    glFogfv(GL_FOG_COLOR, fogColor);
    glFogf(GL_FOG_DENSITY, 0.5);
    glFogf(GL_FOG_START, 0.0f);
    glFogf(GL_FOG_END, 20.0f);
    
    // get ready to render the scene
    glLoadIdentity();
    gluLookAt(0.0f, -10.0f, 10.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
    
    // apply the scene transformations
    glTranslatef(position.x, position.y, position.z);
    
    glRotatef(rotation.pitch, 1.0f, 0.0f, 0.0f);
    glRotatef(rotation.yaw,   0.0f, 1.0f, 0.0f);
    glRotatef(rotation.roll,  0.0f, 0.0f, 1.0f);
    
    // ...and render the scene
    r->render();
    
#ifdef __KAMI_DEBUG__
    glDisable(GL_DEPTH_TEST);
    // render debug objects
    for (auto iter = pdv_objects.begin(); iter != pdv_objects.end(); iter++) {
        pdvobject pdvo = *iter;
        pdvo.render();
    }
    glEnable(GL_DEPTH_TEST);
#endif
    
    glFlush();    
}




