//
//  ai_helpers.h
//  yumi-no-kuni
//
//  Created by John-Evans Wagenaar on 24/05/2013.
//  Copyright (c) 2013 John-Evans Wagenaar. All rights reserved.
//

#ifndef __yumi_no_kuni__ai_helpers__
#define __yumi_no_kuni__ai_helpers__

#include <math.h>
#include "primitives.h"
#include "io.h"


namespace kami
{
    namespace ai_helpers {
        
        // determine the io:: direction from a vector
        /* ======================================= */
        inline uint16_t get_move_dir(const vector &v)
        {
            uint16_t result;
            
            float fy = fabs(v.y);
            float fx = fabs(v.x);
            
            bool px = v.x < 0.0f;
            bool py = v.y < 0.0f;
            
            if (fy > fx) {
                if (py) {
                    result = io::FWD;
                } else {
                    result = io::BCK;
                }
            } else {
                if (px) {
                    result = io::RHT;
                } else {
                    result = io::LFT;
                }
            } 
            
            return result;
        }
        
    }
}

#endif /* defined(__yumi_no_kuni__ai_helpers__) */
