//
//  statemachine.h
//  kami
//
//  Created by John-Evans Wagenaar on 2012/09/08.
//  Copyright (c) 2012 John-Evans Wagenaar. All rights reserved.
//

#ifndef kami_statemachine_h
#define kami_statemachine_h

#include "state.h"

namespace kami
{
    template<class npc_type>
    class statemachine
    {
        npc_type        *m_owner;
        state<npc_type> *m_current;
        state<npc_type> *m_previous;
        state<npc_type> *m_global;
        
    public:
        statemachine(npc_type *owner);
        
        void set_current  (state<npc_type> *st);
        void set_previous (state<npc_type> *st);
        void set_global   (state<npc_type> *st);
        
        state<npc_type> *current();
        state<npc_type> *previous();
        state<npc_type> *global();
        
        void update();
        void revert();
        
        void change   (state<npc_type> *st);
        bool in_state (state<npc_type> *st);
    };
    
    /* ================== */
    template<class npc_type>
    statemachine<npc_type>::statemachine(npc_type *owner) :
    m_owner(owner)
    {}
    
    /* ================== */
    template<class npc_type>
    void statemachine<npc_type>::set_current(state<npc_type> *st)
    {
        m_current = st;
    }
    
    /* ================== */
    template<class npc_type>
    void statemachine<npc_type>::set_previous(state<npc_type> *st)
    {
        m_previous = st;
    }
    
    /* ================== */
    template<class npc_type>
    void statemachine<npc_type>::set_global(state<npc_type> *st)
    {
        m_global = st;
    }
    
    /* ================== */
    template<class npc_type>
    state<npc_type>* statemachine<npc_type>::current()
    {
        return m_current;
    }
    
    /* ================== */
    template<class npc_type>
    state<npc_type>* statemachine<npc_type>::previous()
    {
        return m_previous;
    }
    
    /* ================== */
    template<class npc_type>
    state<npc_type>* statemachine<npc_type>::global()
    {
        return m_global;
    }
    
    /* ================== */
    template<class npc_type>
    void statemachine<npc_type>::update()
    {
        m_current->execute(m_owner);
    }
    
    /* ================== */
    template<class npc_type>
    void statemachine<npc_type>::revert()
    {
        if (m_previous != 0) {
            m_current->exit (m_owner);
            m_current = m_previous;
            m_current->enter(m_owner);
        }
    }
    
    /* ================== */
    template<class npc_type>
    void statemachine<npc_type>::change(state<npc_type> *st)
    {
        if (m_current != 0) {
            m_current->exit(m_owner);
        }
        
        m_previous = m_current;
        m_current  = st;
        m_current->enter(m_owner);
    }
    
    /* ================== */
    template<class npc_type>
    bool statemachine<npc_type>::in_state(state<npc_type> *st)
    {
        return m_current == st;
    }
}




#endif
