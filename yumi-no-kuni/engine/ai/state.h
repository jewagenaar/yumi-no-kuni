//
//  state.h
//  kami
//
//  Created by John-Evans Wagenaar on 2012/09/08.
//  Copyright (c) 2012 John-Evans Wagenaar. All rights reserved.
//

#ifndef kami_state_h
#define kami_state_h

namespace kami
{
    template<class npc_type>
    class state
    {
    public:        
        virtual ~state() {}
        
        virtual void enter   (npc_type *) = 0;
        virtual void execute (npc_type *) = 0;
        virtual void exit    (npc_type *) = 0;
    };
}


#endif
