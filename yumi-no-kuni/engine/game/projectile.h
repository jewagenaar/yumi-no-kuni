#ifndef __yumi_no_kuni__projectile__
#define __yumi_no_kuni__projectile__

#include "kami.h"
#include "renderable.h"
#include "colshapes.h"

namespace kami
{
    class projectile : public renderable
    {
        colthing m_colthing;
        
    public:
        uint32_t owner_id;
        
        point  pos  = { 0.0f, 0.0f, 0.0f };
        vector vel  = { 0.0f, 0.0f, 0.0f };
        
        float  r    = 0.5f;
        bool   hit  = true;
        
        uint32_t dmg = 0;
        
        void render ();
        void step   ();
        
        static projectile *create (const uint32_t &owner, const point &p, const vector &v);
        
        colthing *collision_data (const float &t);
    };
}


#endif /* defined(__yumi_no_kuni__projectile__) */
