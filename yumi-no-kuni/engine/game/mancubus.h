//
//  mancubus.h
//  yumi-no-kuni
//
//  Created by John-Evans Wagenaar on 22/05/2013.
//  Copyright (c) 2013 John-Evans Wagenaar. All rights reserved.
//

#ifndef __yumi_no_kuni__mancubus__
#define __yumi_no_kuni__mancubus__

#include "kami.h"
#include "npc.h"
#include "state.h"
#include "statemachine.h"
#include "hithandler.h"

namespace kami
{
    class mancubus;
    
    namespace mancubus_state
    {
        // chasing state
        class chase : public state<mancubus>
        {
        public:
            static chase *instance();
            
            void enter   (mancubus *actor);
            void execute (mancubus *actor);
            void exit    (mancubus *actor);
        protected:
            chase() {}
        };
        
        // wait state
        class wait : public state<mancubus>
        {
        public:
            static wait *instance();
            
            void enter   (mancubus *actor);
            void execute (mancubus *actor);
            void exit    (mancubus *actor);
        protected:
            wait() {}
        };
        
        // dying state
        class dying : public state<mancubus>
        {
        public:
            static dying *instance();
            
            void enter   (mancubus *actor);
            void execute (mancubus *actor);
            void exit    (mancubus *actor);
        protected:
            dying (void) {};
        };
    }

    // the mancubus npc class
    class mancubus : public npc
    {
        friend mancubus_state::chase;
        friend mancubus_state::wait;
        friend mancubus_state::dying;
        
        static animation *ANIM_STAND;
        static animation *ANIM_WALK;
        static animation *ANIM_DIE;
        
        statemachine<mancubus> *m_brain;
        hithandler<projectile> *m_handler;
        
        uint16_t m_timer;
    public:
        mancubus  (md2model *renderable);
        ~mancubus (void);
        
        void step   (void);
        void stand  (void);
        void move   (const short &dir);
        void attack (void);
        void kill   (void);
        
        void render_model (void);
    };
    
    // the mancubus projectile hit handler
    class mancubus_handler : public hithandler<projectile>
    {
        mancubus *m_mancubus;
        
    public:
        mancubus_handler (mancubus *m) : m_mancubus(m) { }
        
        void handle (projectile *proj)
        {
            proj->hit = true;
            if (m_mancubus->npc_vitals.hitpoints > proj->dmg) {
                m_mancubus->npc_vitals.hitpoints -= proj->dmg;
            } else {
                m_mancubus->npc_vitals.hitpoints = 0;
            }
        }
    };
}

#endif /* defined(__yumi_no_kuni__mancubus__) */
