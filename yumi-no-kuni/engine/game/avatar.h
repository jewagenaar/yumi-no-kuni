#ifndef kami_avatar_h
#define kami_avatar_h

#include <map>
#include "object.h"
#include "kami.h"
#include "renderable.h"
#include "md2model.h"
#include "thing.h"
#include "io.h"
#include "item.h"

#define IPF 0.1

namespace kami
{    
    class avatar : public object
    {
        md2model  *m_model;
        anim_state m_animstate;
        
        thing     *m_thing    = new (std::nothrow) thing;
        bool       m_jumping  = false;
        uint8_t    m_iodir    = io::NIO;
        vector     m_iovector   = { 0.0, 1.0f, 0.0f };
        
        animation *m_animwalk   = new (std::nothrow) anim_loop    (40,  5, IPF);
        animation *m_animjump   = new (std::nothrow) anim_oneshot (51, 20, IPF);
        animation *m_animstand  = new (std::nothrow) anim_loop    (1,  38, IPF);
        animation *m_animattack = new (std::nothrow) anim_oneshot (46,  5, IPF);
        animation *m_current    = m_animstand;

    public:
        
        rotation rot = { 0.0f, 0.0f, 0.0f };
        std::map<uint16_t, uint16_t> inventory;
        
        avatar  (md2model *model);
        ~avatar (void);
        
        void move (const short &dir);
        
        void step   (void);
        void stand  (void);
        void jump   (void);
        void attack (const vector &dir);
        
        void handle_input (const uint16_t &ks);
        
        point    get_position (void);
        rotation get_rotation (void);
        
        void  set_position (const float &x, const float &y, const float &z);
        void  set_position (const point &pos);
        
        thing *physics_thing (void);
        void   set_model_adj (const vector &adj);
        void   render_model  (void);
        
        void   pickup (const uint16_t &it);
    private:
        void set_animation (animation *anim);
    };
}



#endif
