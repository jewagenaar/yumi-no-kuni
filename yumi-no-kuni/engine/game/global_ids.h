#ifndef yumi_no_kuni_stuff_h
#define yumi_no_kuni_stuff_h

#define MODEL_TRIS       0x00
#define MODEL_MASTERMIND 0x01
#define MODEL_MANCUBUS   0x02

#define MODEL_SKYDOME    0x10
#define MODEL_ROOM_9     0x11
#define MODEL_ITEM       0x12

#define IMG_STATUSHUD    0xA0
#define IMG_MENUNEWGAME  0xA1
#define IMG_MENUCURSOR   0xA2
#define IMG_MENUOPTIONS  0xA3
#define IMG_MENUDEBUG    0xA4
#define IMG_MENUBACK     0xA5
#define IMG_MENUQUIT     0xA7

#define IMG_INVENTORY    0xB0
#define IMG_POTION       0xB1
#define IMG_FOOD         0xB2
#define IMG_CRYSTAL      0xB3

#define TEX_TRIS         0x00
#define TEX_GROUND       0x01
#define TEX_SKYDOME      0x02
#define TEX_MASTERMIND   0x03
#define TEX_MANCUBUS     0x04
#define TEX_CRYSTAL      0x05

#endif
