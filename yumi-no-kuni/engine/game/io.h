//
//  io.h
//  kami
//
//  Created by John-Evans Wagenaar on 2012/08/26.
//  Copyright (c) 2012 John-Evans Wagenaar. All rights reserved.
//

#ifndef kami_io_h
#define kami_io_h

#include "../base/kami.h"
#include "../base/primitives.h"

namespace kami
{
    namespace io
    {
        static const uint8_t NIO = 0x00;
        static const uint8_t LFT = 0x01;
        static const uint8_t RHT = 0x02;
        static const uint8_t FWD = 0x04;
        static const uint8_t BCK = 0x08;
        static const uint8_t UP  = 0x10;
        static const uint8_t DWN = 0x20;
        
        static const kami::vector lft_force = {-INPUT_FORCE,  0.0f,        0.0f};
        static const kami::vector rht_force = { INPUT_FORCE,  0.0f,        0.0f};
        static const kami::vector fwd_force = { 0.0f,         INPUT_FORCE, 0.0f};
        static const kami::vector bck_force = { 0.0f,        -INPUT_FORCE, 0.0f};
        
        static const kami::vector lft_dir = {-1.0f,  0.0f, 0.0f};
        static const kami::vector rht_dir = { 1.0f,  0.0f, 0.0f};
        static const kami::vector fwd_dir = { 0.0f,  1.0f, 0.0f};
        static const kami::vector bck_dir = { 0.0f, -1.0f, 0.0f};
    }
}

#endif
