#ifndef __yumi_no_kuni__gamedata__
#define __yumi_no_kuni__gamedata__

#include "kami.h"

namespace kami
{
    class vitals
    {
    public:
        static const uint8_t DEAD   = 0;
        static const uint8_t KILLED = 1;
        static const uint8_t ALIVE  = 2;
        
        uint32_t hitpoints = 0;
        uint32_t mana      = 0;
        uint8_t  stamina   = 0;
        uint8_t  level     = 0;
        uint8_t  state     = DEAD;
    };
}

#endif /* defined(__yumi_no_kuni__gamedata__) */
