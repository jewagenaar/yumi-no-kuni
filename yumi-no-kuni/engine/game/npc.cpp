#include "npc.h"
#include "io.h"

#include "kami.h"
#include "kamimath.h"

using namespace kami;

npc::npc(md2model *model) :
m_thing        (new (std::nothrow) thing),
m_model        (model),
m_jumping      (false),
m_iodir        (io::NIO)
{
    INIT_VOP(m_iovector, 0.0f);
    m_thing->colradius = 1.5f;
    sr                 = 1.1f;
}

/* ===== */
npc::~npc()
{
    delete m_thing;
    m_thing = NULL;
}

/* ===================== */
thing *npc::physics_thing()
{
    return m_thing;
}

/* ================================================================ */
void npc::set_position(const float &x, const float &y, const float &z)
{
    m_thing->set_pos(x, y, z);
}

/* ================================== */
void npc::set_position(const point &pos)
{
    m_thing->position = pos;
}

/* =================== */
point npc::get_position()
{
    return m_thing->position;
}

/* ====================== */
rotation npc::get_rotation()
{
    return rot;
}

/* ============================= */
void npc::move(const uint8_t &dir)
{
    INIT_VOP(m_iovector, 0.0f);
    
    if(m_thing->freefall) {
        return;
    }
    
    kami::vector app_force = NULL_VEC;
    
    if (dir & io::FWD) {
        vmath::inline_add(m_iovector, io::fwd_dir);
        vmath::inline_add(app_force,  io::fwd_force);
    }
    
    if (dir & io::BCK) {
        vmath::inline_add(m_iovector, io::bck_dir);
        vmath::inline_add(app_force,  io::bck_force);
    }
    
    if (dir & io::LFT) {
        vmath::inline_add(m_iovector, io::lft_dir);
        vmath::inline_add(app_force,  io::lft_force);
    }
    
    if (dir & io::RHT) {
        vmath::inline_add(m_iovector, io::rht_dir);
        vmath::inline_add(app_force,  io::rht_force);
    }
    
    m_thing->force = vmath::smult(NPCIN_FORCE, vmath::normalize(app_force));
    
    float angle = vmath::calc_rot(m_iovector);
    rot.roll = angle;
}

/* ================================== */
void npc::set_animation(animation *anim)
{
    if(m_current != anim) {
        m_animstate.cfi = anim->si;
        m_animstate.ipf = 0.0f;
        m_animstate.fin = false;
        
        m_current = anim;
    }
}

/* ======================== */
kami::vector npc::direction()
{
    return m_thing->direction;
}

