#ifndef kami_npc_h
#define kami_npc_h

#include <map>
#include "object.h"
#include "thing.h"
#include "primitives.h"
#include "md2model.h"
#include "statemachine.h"
#include "animation.h"
#include "gamedata.h"
#include "item.h"

namespace kami
{
    class aicontrol;
    
    class npc : public object
    {
        friend aicontrol;
        
        thing   *m_thing;
        bool     m_jumping;
        uint8_t  m_iodir;
        vector   m_iovector;
    
    protected:
        md2model   *m_model;
        anim_state  m_animstate;
        animation  *m_current;
        
    public:
        std::map<uint16_t, uint16_t> inventory;
        
        rotation rot;
        vitals   npc_vitals;
        
        npc  (md2model *model);
        ~npc (void);
        
        thing *physics_thing (void);
        
        virtual void stand  (void) = 0;
        virtual void attack (void) = 0;
        virtual void step   (void) = 0;
        virtual void kill   (void) = 0;
        
        virtual void move (const uint8_t &dir);
        
        // template getter methods from object
        point    get_position (void);
        rotation get_rotation (void);
        
        void  set_position  (const float &x, const float &y, const float &z);
        void  set_position  (const point &pos);
        void  set_animation (animation *anim);
        
        vector direction (void);
    };
}



#endif
