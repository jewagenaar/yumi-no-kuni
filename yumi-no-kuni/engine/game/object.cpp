#include <iostream>
#include "object.h"

using namespace kami;

/* =============== */
void object::render()
{
    float sr_max = 1.22 * sr;
    
    point    pos = get_position();
    rotation rot = get_rotation();
    
    glPushMatrix();
    glTranslatef(pos.x, pos.y, 0.001f);
    
    // draw a shadow on the floor
    glUseProgram(0);
    glEnable(GL_BLEND);
    glBlendEquation(GL_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4f(0.0f, 0.0f, 0.0f, 0.55f);

    glBegin(GL_POLYGON);
    
    glVertex2f(-sr,     sr);
    glVertex2f(-sr_max, 0.00f);
    
    glVertex2f(-sr,    -sr);
    glVertex2f( 0.00f, -sr_max);
    
    glVertex2f( sr,     -sr);
    glVertex2f( sr_max,  0.00f);
    
    glVertex2f( sr,    sr);
    glVertex2f( 0.00f, sr_max);
    
    glEnd();
    glDisable(GL_BLEND);
    
    // now set the object's z pos and draw
    glTranslatef(0.0f, 0.0f, pos.z);
    glRotatef(rot.pitch, 1.0f, 0.0f, 0.0f);
    glRotatef(rot.yaw,   0.0f, 1.0f, 0.0f);
    glRotatef(rot.roll,  0.0f, 0.0f, 1.0f);
    render_model();
    glPopMatrix();
    
}
