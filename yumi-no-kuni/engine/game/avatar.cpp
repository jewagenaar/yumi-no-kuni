//
//  avatar.cpp
//  kami
//
//  Created by John Evans Wagenaar on 2012/03/21.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include "../base/kami.h"
#include "../base/kamimath.h"
#include "avatar.h"
#include "controller.h"
#include <math.h>

using namespace kami;

avatar::avatar(md2model *model) : m_model (model)
{
    m_thing->colradius = 0.5;
    
    kami::vector adj   = { 0.0, 0.0, 0.65 };
    m_model->adjust    = adj;
    
    inventory[ITEM_COIN]++;
    inventory[ITEM_FOOD]++;
    inventory[ITEM_POTION]++;
}

/* =========== */
avatar::~avatar()
{
    delete m_animstand;
    m_animstand = 0;
    
    delete m_animjump;
    m_animjump = 0;
    
    delete m_animwalk;
    m_animwalk = 0;
    
    delete m_animattack;
    m_animattack = 0;
    
    delete m_thing;
    m_thing = 0;
}

/* ====================== */
point avatar::get_position()
{
    return m_thing->position;
}

/* ========================= */
rotation avatar::get_rotation()
{
    return rot;
}

/* ============== */
void avatar::stand()
{
    controller::instance()->the_hud->debug(KAMI_DL_AVATAR, "STD");
    m_iodir = io::NIO;
    
    set_animation(m_animstand);
}

/* ============================= */
void avatar::move(const short &dir)
{
    if (dir == io::NIO) {
        return;
    }
    
    if(m_thing->freefall) {
        return;
    }
    
    INIT_VOP(m_iovector, 0.0f);
    
    vector app_force = NULL_VEC;
    
    if (dir & io::FWD) {
        controller::instance()->the_hud->debug(KAMI_DL_AVATAR, "FWD");
        vmath::inline_add(m_iovector, io::fwd_dir);
        vmath::inline_add(app_force,  io::fwd_force);
    }
    
    if (dir & io::BCK) {
        controller::instance()->the_hud->debug(KAMI_DL_AVATAR, "BCK");
        vmath::inline_add(m_iovector, io::bck_dir);
        vmath::inline_add(app_force,  io::bck_force);
    }
    
    if (dir & io::LFT) {
        controller::instance()->the_hud->debug(KAMI_DL_AVATAR, "LFT");
        vmath::inline_add(m_iovector, io::lft_dir);
        vmath::inline_add(app_force,  io::lft_force);
    }
    
    if (dir & io::RHT) {
        controller::instance()->the_hud->debug(KAMI_DL_AVATAR, "RHT");
        vmath::inline_add(m_iovector, io::rht_dir);
        vmath::inline_add(app_force,  io::rht_force);
    }
    
    app_force = vmath::smult(INPUT_FORCE, vmath::normalize(app_force));
    vmath::inline_add(m_thing->force, app_force);
    
    set_animation(m_animwalk);
    
    float angle = vmath::calc_rot(m_iovector);
    rot.roll    = angle;
}

/* ============= */
void avatar::jump()
{
    set_animation(m_animjump);
}

/* ======================================== */
void avatar::handle_input (const uint16_t &ks)
{
    if(!ks) {
        stand();
        return;
    }
    
    // initialize the current input direction
    short dir = io::NIO;
    
    // if player pressed UP go FORWARD
    if(ks & hidaction::MV_UP) {
        dir |= io::FWD;
    }
    
    // if player pressed DOWN go BACK
    if(ks & hidaction::MV_DWN) {
        dir |= io::BCK;
    }
    
    // if player pressed LEFT go LEFT
    if(ks & hidaction::MV_LFT) {
        dir |= io::LFT;
    }
    
    // if player pressed RIGHT go RIGHT
    if(ks & hidaction::MV_RHT) {
        dir |= io::RHT;
    }
    
    // if player pressed FIRE, we create a projectile
    if (ks & hidaction::SHT_LFT) {
        attack(io::lft_dir);
    } else if (ks & hidaction::SHT_RHT) {
        attack(io::rht_dir);
    } else if (ks & hidaction::SHT_UP) {
        attack(io::fwd_dir);
    } else if (ks & hidaction::SHT_DWN) {
        attack(io::bck_dir);
    }
    
    // pass the direction data to the avatar
    move(dir);
}

/* ============= */
void avatar::step()
{
    m_current->step (m_animstate);
}

/* =============================== */
void avatar::attack(const vector &dir)
{
    static time_point last = KAMI_TIME_NOW;
    time_point now         = KAMI_TIME_NOW;
    long d                 = KAMI_TIME_FINE(last, now);
    
    if (d > 400) {
        last = KAMI_TIME_NOW;
    } else {
        return;
    }
    
    point  pos = get_position();
    pos.z += 1.0f;
    uint8_t id = m_thing->ID;
    
    projectile *proj = projectile::create(id, pos, dir);
    proj->dmg = 70;
    
    if (proj) {
        controller::instance()->the_sector->projectiles.push_back(proj);
    }
}

/* =================================================================== */
void avatar::set_position(const float &x, const float &y, const float &z)
{
    m_thing->set_pos(x, y, z);
}

/* =========================================== */
void avatar::set_position(const kami::point &pos)
{
    m_thing->position = pos;
}

/* ========================= */
thing * avatar::physics_thing()
{
    return m_thing;
}

/* ======================================= */
void avatar::set_model_adj(const vector &adj)
{
    m_model->adjust = adj;
}

/* ===================== */
void avatar::render_model()
{
    m_model->render(m_animstate);
}

/* ================================== */
void avatar::pickup (const uint16_t &it)
{
    // increase the amount held for this item
    inventory[it]++;
}

/* ===================================== */
void avatar::set_animation(animation *anim)
{
    if(m_current != anim) {
        m_animstate.cfi = anim->si;
        m_animstate.ipf = 0.0f;
        
        m_current = anim;
    }
}
