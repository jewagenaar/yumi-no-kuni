//
//  mastermind.cpp
//  kami
//
//  Created by John-Evans Wagenaar on 2012/08/02.
//  Copyright (c) 2012 John-Evans Wagenaar. All rights reserved.
//

#include "mastermind.h"
#include "avatar.h"
#include "kamimath.h"
#include "controller.h"
#include "ai_helpers.h"

using namespace kami;

animation *mastermind::ANIM_STAND  = new (std::nothrow) anim_loop    (0,  2, 0.05f);
animation *mastermind::ANIM_WALK   = new (std::nothrow) anim_loop    (2,  7, 0.05f);
animation *mastermind::ANIM_ATTACK = new (std::nothrow) anim_loop    (7,  5, 0.05f);
animation *mastermind::ANIM_DIE    = new (std::nothrow) anim_oneshot (7,  5, 0.05f);

mastermind::mastermind(md2model *model) :
npc         (model),
m_brain     (new (std::nothrow) statemachine<mastermind>(this)),
m_handler   (new (std::nothrow) mastermind_handler(this)),
m_timer     (0)
{
    m_model         = model;
    vector adj      = NULL_VEC;
    m_model->adjust = adj;
    
    set_animation(ANIM_STAND);
    
    m_brain->set_current(mastermind_state::wait::instance());
    
    physics_thing()->handler = m_handler;
    
    npc_vitals.hitpoints = 200;
    npc_vitals.state     = vitals::ALIVE;
}

/* =================== */
mastermind::~mastermind()
{

}

/* ================= */
void mastermind::step()
{
    m_brain->update();
    m_current->step(m_animstate);
}

/* ================== */
void mastermind::stand()
{    
    set_animation(ANIM_STAND);
}

/* ==================================== */
void mastermind::move(const uint16_t &dir)
{
    npc::move(dir);
    set_animation(ANIM_WALK);
}

/* =================== */
void mastermind::attack()
{
    set_animation(ANIM_ATTACK);
}

/* ================= */
void mastermind::kill()
{
    set_animation(ANIM_DIE);
    m_brain->change(mastermind_state::dying::instance());
}

/* ========================= */
void mastermind::render_model()
{
    m_model->render(m_animstate);
}

// === state machine states

// --- chase state

/* ====================================================== */
mastermind_state::chase *mastermind_state::chase::instance()
{
    chase *__instance = new chase;
    return __instance;
}

/* ================================================ */
void mastermind_state::chase::enter(mastermind *actor)
{

}

/* ================================================== */
void mastermind_state::chase::execute(mastermind *actor)
{
    avatar *_avatar = controller::instance()->the_avatar;
    vector dir = pmath::vector_from_point(_avatar->get_position(), actor->get_position());
    float dist = vmath::len(dir);
    
    if(dist > 4.0f && dist < 8.0f) {
        vector dir     = pmath::vector_from_point(_avatar->get_position(), actor->get_position());
        uint8_t io_dir = ai_helpers::get_move_dir(dir);
        
        actor->move(io_dir);
    } else {
        actor->m_brain->set_current(mastermind_state::wait::instance());
    }
}

/* =============================================== */
void mastermind_state::chase::exit(mastermind *actor)
{
    
}

// --- wait state

/* ==================================================== */
mastermind_state::wait *mastermind_state::wait::instance()
{
    wait *__instance = new wait;
    return __instance;
}

/* =============================================== */
void mastermind_state::wait::enter(mastermind *actor)
{
    actor->m_timer = 0;
    actor->stand();
}

/* ================================================= */
void mastermind_state::wait::execute(mastermind *actor)
{
    actor->m_timer++;
    if(actor->m_timer > 100) {
        actor->m_brain->set_current(mastermind_state::chase::instance());
    }
}

/* ============================================== */
void mastermind_state::wait::exit(mastermind *actor)
{
    actor->m_timer = 0;
}

// --- dying state

/* ====================================================== */
mastermind_state::dying *mastermind_state::dying::instance()
{
    dying *__instance = new dying;
    return __instance;
}

/* ================================================= */
void mastermind_state::dying::enter (mastermind *actor)
{
    
}

/* =================================================== */
void mastermind_state::dying::execute (mastermind *actor)
{
    if (actor->m_animstate.fin) {
        actor->npc_vitals.state = vitals::DEAD;
    }
}

/* ================================================ */
void mastermind_state::dying::exit (mastermind *actor)
{
    
}


