#ifndef kami_object_h
#define kami_object_h

#include "kami.h"
#include "renderable.h"
#include "primitives.h"

/*
 * Object are representations of physical objects in the game world.
 * Things that can be seen, bumped into, picked up, moved around etc.
 */
namespace kami
{
    class object : public renderable
    {
        
    public:
        
        float sr = 0.5f;
        
        void render ();
        
        virtual point    get_position() = 0;
        virtual rotation get_rotation() = 0;
        
    protected:
        virtual void render_model() = 0;
    };
}


#endif
