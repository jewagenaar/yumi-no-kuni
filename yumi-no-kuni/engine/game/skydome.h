//
//  skydome.h
//  kami
//
//  Created by John-Evans Wagenaar on 2012/08/05.
//  Copyright (c) 2012 John-Evans Wagenaar. All rights reserved.
//

#ifndef __kami__skydome__
#define __kami__skydome__

#include <iostream>
#include "../scene/renderable.h"
#include "../scene/wfmodeldata.h"

namespace kami
{
    class skydome : public renderable
    {
        wfmodel *m_model;
    public:
        point position;
        
        skydome(wfmodel *model);
        
        void render();
    };
}




#endif /* defined(__kami__skydome__) */
