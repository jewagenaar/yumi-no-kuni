#ifndef __yumi_no_kuni__mainmenu__
#define __yumi_no_kuni__mainmenu__

#include "hudlayer.h"

namespace kami
{
    class menuitem;
    
    // menu meta data
    class menu
    {
    public:
        short    cur = 0;
        uint16_t num = 0;
        
        menuitem **entries;
        
         menu (menuitem **e, const uint16_t &n);
        ~menu (void);
    };
    
    // menu item entry
    class menuitem
    {
    public:
        sprite    *spr;
        menu      *submenu = 0;
        uint16_t   value;
        
         menuitem (const uint16_t &img_id, const point &pos);
        ~menuitem (void);
    };
    
    // renderable menu
    class mainmenu : public hudlayer
    {
        menu *m_active;
        menu *m_home;
        menu *m_suboptions;
        
        menuitem *m_newgame;
        menuitem *m_options;
        menuitem *m_quit;
        menuitem *m_volume;
        menuitem *m_back;
        
        sprite   *m_cursor;
        
        uint16_t m_timeout = 0;
        
    public:
        static const uint16_t DONE   = 0x00;
        static const uint16_t ACTIVE = 0x01;
        
        static const uint16_t NONE    = 0x00;
        static const uint16_t NEWGAME = 0x01;
        static const uint16_t QUIT    = 0x02;
        static const uint16_t VOLUME  = 0x03;
        static const uint16_t BACK    = 0x04;
        
        uint16_t state     = DONE;
        uint16_t selection = NONE;
        
         mainmenu (void);
        ~mainmenu (void);
        
        void reset (void);
        void step  (void);
        
        void handle_input (const uint16_t &ks);
        
    private:
        void select_next (void);
        void select_prev (void);
        
        void execute (void);
        
        void set_active (menu *m);
    };
}

#endif /* defined(__yumi_no_kuni__mainmenu__) */
