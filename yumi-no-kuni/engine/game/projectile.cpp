#include "projectile.h"
#include "kamimath.h"
#include <GLUT/GLUT.h>

using namespace kami;

projectile *projectile::create (const uint32_t &owner, const point &p, const vector &v)
{
    // our local heap store for projectiles
    static projectile _pstore [50];
    
    for (int i = 0; i < 50; i++) {
        if (_pstore[i].hit) {
            // we assign from our local heap
            projectile *result = new (&_pstore[i]) projectile;

            // setup the new projectile
            result->owner_id   = owner;
            result->hit        = false;
            result->pos        = p;
            result->vel        = v;
            
            return result;
        }
    }
    
    return 0;
}

/* =================== */
void projectile::render()
{
    glUseProgram    (0);
    glPushMatrix    ();
    glColor3f       (0.6f, 0.0f, 0.0f);
    glTranslatef    (pos.x, pos.y, pos.z);
    glutSolidSphere (r, 8, 8);
    glPopMatrix     ();
}

/* ================= */
void projectile::step()
{
    pos = m_colthing.end;
}

/* ============================================== */
colthing *projectile::collision_data(const float &t)
{
    vector disp = vmath::smult     (t, vel);
    point  end  = pmath::translate (pos, disp);
    
    m_colthing.dir   = vmath::normalize(vel);
    m_colthing.start = pos;
    m_colthing.end   = end;
    m_colthing.r     = r;
    
    return &m_colthing;
}