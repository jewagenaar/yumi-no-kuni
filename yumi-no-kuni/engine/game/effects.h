#ifndef __yumi_no_kuni__explosion__
#define __yumi_no_kuni__explosion__

#include "kami.h"
#include "primitives.h"
#include "renderable.h"

namespace kami
{
    // forward declarations -- all hail the spaghetti monster
    class effect_state;
    class explosion;
    class splat;
    class boom_splat;
    
    // =====
    class effect
    {
    public:
        static explosion  *EXPLOSION;
        static splat      *SPLAT;
        static boom_splat *BOOM_SPLAT;
        
        static effect_state* create(const point &pos, effect *eff);
        
        virtual void step   (effect_state *es) = 0;
        virtual void render (effect_state *es) = 0;
    };

    // =====
    class effect_state : public renderable
    {
    public:
        effect   *eff = 0;                    // the effect that is being played
        uint32_t  cur = 0;                    // the current frame
        bool      fin = true;                 // the status
        point     pos = { 0.0f, 0.0f, 0.0f }; // the position
        
        void step (void)
        {
            eff->step(this);
        }
        
        void render (void)
        {
            eff->render(this);
        }
    };
    
    // ======
    class explosion : public effect
    {
    public:
        uint8_t m_fc = 20; // the frame count
        
        void step   (effect_state *es);
        void render (effect_state *es);
    };
    
    // ======
    class splat : public effect
    {
    public:
        uint8_t m_fc = 255;
        
        void step   (effect_state *es);
        void render (effect_state *es);
    };
    
    // ======
    class boom_splat : public effect
    {
    public:
        void step   (effect_state *es);
        void render (effect_state *es);
    };
}

#endif /* defined(__yumi_no_kuni__explosion__) */
