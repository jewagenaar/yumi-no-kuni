#include "skydome.h"

using namespace kami;

/* ========================== */
skydome::skydome(wfmodel *model) :
m_model(model)
{
    INIT_VOP(position, 0.0f);
}

/* ================ */
void skydome::render()
{
    m_model->render();
}
