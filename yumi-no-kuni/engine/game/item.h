#ifndef __yumi_no_kuni__item__
#define __yumi_no_kuni__item__

#include <string>
#include "kami.h"
#include "primitives.h"
#include "object.h"
#include "hithandler.h"
#include "wfmodeldata.h"
#include "sprite.h"
#include "texman.h"
#include "global_ids.h"

#define ITEM_POTION 0x01
#define ITEM_COIN   0x02
#define ITEM_FOOD   0x03

namespace kami
{
    class avatar;
    
    // scene prop
    class prop : public object
    {
    public:
        float     r     = 0.5f; // collision radius
        rgb_color c     = { 1.0f, 1.0f, 1.0f };
        point     pos   = { 0.0f, 0.0f, 0.0f };
        rotation  rot   = { 0.0f, 0.0f, 0.0f };
        bool      hit   = false; // set if it was hit by avatar/npc
        bool      dirty = true;  // set if needs to be disposed
        
        hithandler<avatar> *handler;
        wfmodel            *model;
        
        ~prop (void);
        
        // template methods from object
        point    get_position (void);
        rotation get_rotation (void);
    protected:
        void render_model (void);
    };
    
    /* === COIN ITEM === */
    
    // coin prop
    class coin_prop : public prop
    {
    public:
        static prop *create(const point &pos);
    private:
        coin_prop (const point &pos);
    };
    
    // coin handler
    class coin_handler : public hithandler<avatar>
    {
    public:
        void handle (avatar *a);
    };
    
    // coin bag item
    class coin_item
    {
    public:
        static sprite *get_sprite (void)
        {
            static sprite *_sprite = create_sprite();
            return _sprite;
        }
    private:
        static sprite *create_sprite (void)
        {
            sprite *spr = new sprite;
            spr->texid  = texman::instance()->get_texture(IMG_CRYSTAL);
            spr->height =  0.14f;
            spr->width  =  0.14f;
            spr->pos.x  =  0.53f;
            spr->pos.y  =  0.97f;
            spr->pos.z  = -1.00f;
            return spr;
        }
    };
    
    /* === POTION ITEM === */
    
    // potion prop item
    class potion_prop : public prop
    {
    public:
        static prop *create (const point &pos);
    private:
        potion_prop (const point &pos);
    };
    
    // coin handler
    class potion_handler : public hithandler<avatar>
    {
    public:
        void handle (avatar *a);
    };
    
    // coin bag item
    class potion_item
    {
    public:
        static sprite *get_sprite (void)
        {
            static sprite *_sprite = create_sprite();
            return _sprite;
        }
    private:
        static sprite *create_sprite (void)
        {
            sprite *spr = new sprite;
            spr->texid  = texman::instance()->get_texture(IMG_POTION);
            spr->height =  0.14f;
            spr->width  =  0.14f;
            spr->pos.x  =  0.67f;
            spr->pos.y  =  0.97f;
            spr->pos.z  = -1.00f;

            return spr;
        }
    };
    
    /* === FOOD ITEM === */
    
    // food prop item
    class food_prop : public prop
    {
    public:
        static prop *create (const point &pos);
    private:
        food_prop (const point &pos);
    };
    
    // coin handler
    class food_handler : public hithandler<avatar>
    {
    public:
        void handle (avatar *a);
    };
    
    // coin bag item
    class food_item
    {
    public:
        static sprite *get_sprite (void)
        {
            static sprite *_sprite = create_sprite();
            return _sprite;
        }
    private:
        static sprite *create_sprite (void)
        {
            sprite *spr = new sprite;
            spr->texid  = texman::instance()->get_texture(IMG_FOOD);
            spr->height =  0.14f;
            spr->width  =  0.14f;
            spr->pos.x  =  0.81f;
            spr->pos.y  =  0.97f;
            spr->pos.z  = -1.00f;
            
            return spr;
        }
    };
}

#endif /* defined(__yumi_no_kuni__item__) */
