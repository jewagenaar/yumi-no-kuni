//
//  mastermind.h
//  kami
//
//  Created by John-Evans Wagenaar on 2012/08/02.
//  Copyright (c) 2012 John-Evans Wagenaar. All rights reserved.
//

#ifndef __kami__mastermind__
#define __kami__mastermind__

#include "npc.h"
#include "animation.h"
#include "state.h"
#include "statemachine.h"
#include "hithandler.h"

namespace kami
{
    class mastermind;
    
    // states for the mastermind's statemachine
    namespace mastermind_state
    {
        // chasing state
        class chase : public state<mastermind>
        {
        public:
            static chase *instance();
            
            void enter   (mastermind *actor);
            void execute (mastermind *actor);
            void exit    (mastermind *actor);
        protected:
            chase (void) {}
        };
        
        // waiting state
        class wait : public state<mastermind>
        {
        public:
            static wait *instance();
            
            void enter   (mastermind *actor);
            void execute (mastermind *actor);
            void exit    (mastermind *actor);
        protected:
            wait (void) {}
        };
        
        // dying state
        class dying : public state<mastermind>
        {
        public:
            static dying *instance();
            
            void enter   (mastermind *actor);
            void execute (mastermind *actor);
            void exit    (mastermind *actor);
        protected:
            dying (void) {};
        };
    }
    
    // the mastermind npc implementation
    class mastermind : public npc
    {
        friend mastermind_state::wait;
        friend mastermind_state::chase;
        friend mastermind_state::dying;
        
        static animation *ANIM_STAND;
        static animation *ANIM_WALK;
        static animation *ANIM_ATTACK;
        static animation *ANIM_DIE;
 
        statemachine<mastermind> *m_brain;
        hithandler<projectile>   *m_handler;
        
        uint16_t m_timer;
        
    public:
        
        mastermind(md2model *renderable);
        ~mastermind();
        
        void step   (void);
        void stand  (void);
        void move   (const uint16_t &dir);
        void attack (void);
        void kill   (void);
        
        void render_model(void);
    };
    
    // the mastermind projectile hit handler
    class mastermind_handler : public hithandler<projectile>
    {
        mastermind *m_mastermind;
        
    public:
        mastermind_handler (mastermind *m) : m_mastermind(m) { }
        
        void handle (projectile *proj)
        {
            proj->hit = true;
            if (m_mastermind->npc_vitals.hitpoints > proj->dmg) {
                m_mastermind->npc_vitals.hitpoints -= proj->dmg;
            } else {
                m_mastermind->npc_vitals.hitpoints = 0;
            }
        }
    };
}



#endif /* defined(__kami__mastermind__) */
