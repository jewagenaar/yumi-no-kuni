#include <new>

#include "menu.h"
#include "global_ids.h"
#include "hidaction.h"
#include "texman.h"

using namespace kami;

menu::menu (menuitem **e, const uint16_t &n)
{
    num     = n;
    entries = e;
}

/* ============ */
menu::~menu (void)
{
    delete entries;
}

/* ======================================================= */
menuitem::menuitem (const uint16_t &img_id, const point &pos)
{
    spr = new sprite ();
    spr->width  = 1.00f;
    spr->height = 0.20f;
    spr->texid  = texman::instance()->get_texture(img_id);
    spr->pos    = pos;
}

/* ==================== */
menuitem::~menuitem (void)
{
    
}

/* =============== */
mainmenu::mainmenu ()
{
    menuitem **homeitems = static_cast<menuitem**>(::operator new (3 * sizeof(menuitem*)));
    
    point pos_1 = { -0.5f, 0.40f, -1.1f };
    point pos_2 = { -0.5f, 0.20f, -1.1f };
    point pos_3 = { -0.5f, 0.00f, -1.1f };
    
    m_newgame = new (std::nothrow) menuitem (IMG_MENUNEWGAME, pos_1);
    m_newgame->value = NEWGAME;
    homeitems [0] = m_newgame;
    
    m_options = new (std::nothrow) menuitem (IMG_MENUOPTIONS, pos_2);
    homeitems [1] = m_options;
    
    m_quit = new (std::nothrow) menuitem (IMG_MENUQUIT, pos_3);
    m_quit->value = QUIT;
    homeitems [2] = m_quit;
    
    m_home = new (std::nothrow) menu (homeitems, 3);
    
    menuitem ** optionitems = static_cast<menuitem **>(::operator new (2 * sizeof(menuitem *)));

    m_volume = new (std::nothrow) menuitem (IMG_MENUDEBUG, pos_1);
    m_volume->value = VOLUME;
    optionitems [0] = m_volume;
    
    m_back = new (std::nothrow) menuitem (IMG_MENUBACK, pos_2);
    m_back->submenu = m_home;
    optionitems [1] = m_back;
    
    m_suboptions = new (std::nothrow) menu (optionitems, 2);
    
    m_options->submenu = m_suboptions;
    m_back->submenu = m_home;
    
    m_cursor = new sprite ();
    m_cursor->texid  = texman::instance()->get_texture(IMG_MENUCURSOR);
    m_cursor->height = 0.20f;
    m_cursor->width  = 0.20f;
    m_cursor->pos.x  =  -0.7f;
    m_cursor->pos.y  =   0.4f;
    m_cursor->pos.z  =  -1.0f;
    
    set_active(m_home);
}

/* ==================== */
mainmenu::~mainmenu (void)
{
    delete m_newgame;
    delete m_options;
    delete m_quit;
    delete m_volume;
    delete m_back;
    
    delete m_cursor;
    
    delete m_home;
}

/* ================= */
void mainmenu::reset ()
{
    state = ACTIVE;
}

/* ================ */
void mainmenu::step ()
{
    if (m_timeout) {
        m_timeout--;
    }
}

/* ========================================= */
void mainmenu::handle_input(const uint16_t &ks)
{
    if (m_timeout) {
        return;
    }
    
    if (ks & hidaction::SHT_DWN) {
        m_timeout = 20;
        select_next();
    } else if (ks & hidaction::SHT_UP) {
        m_timeout = 20;
        select_prev();
    } else if (ks & hidaction::USE) {
        m_timeout = 20;
        execute();
    }
}

/* =========================== */
void mainmenu::select_next (void)
{
    m_active->cur++;
    
    if (m_active->cur >= m_active->num) {
        m_active->cur = 0;
        m_cursor->pos.y = 0.4f;
    } else {
        m_cursor->pos.y -= 0.2f;
    }
}

/* =========================== */
void mainmenu::select_prev (void)
{
    m_active->cur--;
    if (m_active->cur < 0) {
        m_active->cur = m_active->num - 1;
        m_cursor->pos.y = 0.4f - (m_active->num - 1) * 0.2f;
    } else {
        m_cursor->pos.y += 0.2f;
    }
}

/* ======================= */
void mainmenu::execute (void)
{
    menuitem *i = m_active->entries[m_active->cur];
    
    if (i->submenu) {
        set_active (i->submenu);
    } else {
        selection = i->value;
        state = DONE;
    }
}

/* ============================= */
void mainmenu::set_active (menu *m)
{
    m_active = m;
    
    hudlayer::clear ();
    for (int i = 0; i < m_active->num; i++) {
        hudlayer::add(m_active->entries[i]->spr);
    }
    
    m_active->cur = 0;
    m_cursor->pos.y = 0.4f;
    hudlayer::add(m_cursor);
}

