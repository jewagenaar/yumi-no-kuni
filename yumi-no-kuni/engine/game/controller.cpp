#include "kami.h"
#include "kamimath.h"
#include "shaderman.h"
#include "modelman.h"
#include "texman.h"
#include "global_ids.h"

#include "bboxman.h"
#include "imgman.h"
#include "sprite.h"
#include "primitives.h"
#include "controller.h"
#include "sector.h"

#include "io.h"

using namespace kami;

controller *controller::instance()
{
    static controller * _instance = new controller();
    return _instance;
}

/* ================== */
controller::controller()
{
}

/* =================== */
controller::~controller()
{
    delete the_hid;
    delete the_hud;
    delete the_avatar;

    delete m_world;
    delete m_scene;
}

/* ================= */
void controller::step()
{
    // handle user input
    user_input();
    
    switch (m_state) {
        case GAME:
            world_update();
            break;
        case MENU:
            process_menu();
            break;
        default:
            break;
    }
    
    // get the current sector from the world
    the_sector = m_world->current_sector;
    
    // set the sector to render into the scene
    m_scene->r = the_sector;
}

/* =================== */
void controller::render()
{    
    // then render
    m_scene->render();
    
    if (m_state == MENU) {
        m_menu->render();
    } else {
        the_hud->render();
    }
}

/* ========================= */
void controller::world_update()
{
    m_world->step(m_time);
    
    the_avatar->step();
    the_sector->step();
    
    the_hud->step();
    
    m_scene->position.x = -the_avatar->get_position().x;
    m_scene->position.y = -the_avatar->get_position().y;
}

/* ========================= */
void controller::process_menu()
{
    if (m_menu->state == mainmenu::DONE) {
        switch (m_menu->selection) {
            case mainmenu::QUIT:
                m_state = QUIT;
                break;
            case mainmenu::NEWGAME:
                m_menu->reset();
                m_state = GAME;
                break;
            default:
                m_menu->reset();
                break;
        }
    } else {
        m_menu->step();
    }
}

/* ===================== */
void controller::shutdown()
{
    modelman::instance()->free_all();
    texman  ::instance()->free_all();
    imgman  ::instance()->free_all();
    bboxman ::instance()->free_all();
}

/* ================= */
void controller::init()
{
    // setup the world
    m_world->current_sector = the_sector;
    m_world->the_avatar     = the_avatar;
    
    // initialize stuff that depends on resources
    the_hud = new (std::nothrow) ingame_hud();
    m_menu  = new (std::nothrow) mainmenu;
    m_menu->state = mainmenu::ACTIVE;
    
    ready = true;
    m_state = MENU;
}

/* ======================= */
void controller::user_input()
{
    //get the state of the keyboard
    uint16_t ks = the_hid->keystate;
    
    m_pausetimeout > 0 ? m_pausetimeout-- : m_pausetimeout;
    
    if (ks & hidaction::PAUSE && !m_pausetimeout) {
        m_pausetimeout = 10;
        if (m_state == GAME) {
            m_state = MENU;
        } else {
            m_state = GAME;
        }
        return;
    }
    
    switch (m_state) {
        case MENU:
            m_menu->handle_input(ks);
            break;
        case GAME:
            the_avatar->handle_input(ks);
            break;
        default:
            break;
    }
}
