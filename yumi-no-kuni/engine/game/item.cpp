#include <new>
#include "item.h"
#include "avatar.h"

#define MAX_PROPS 256

using namespace kami;

// prop memory
prop PROP_MEMORY [MAX_PROPS];

/* ============ */
prop::~prop (void)
{
    
}

/* =================== */
void prop::render_model()
{
    glUseProgram    (0);
    glPushMatrix    ();
    glColor3f       (c.r, c.g, c.b);
    glutSolidSphere (0.5f, 8, 8);
    glPopMatrix     ();
}

/* ==================== */
point prop::get_position()
{
    return pos;
}

/* ======================= */
rotation prop::get_rotation()
{
    return rot;
}

/* === COIN ITEMS === */

/* ================================= */
prop* coin_prop::create (const point &p)
{
    prop *result = 0;
    for (int i = 0; i < MAX_PROPS; i++) {
        if (PROP_MEMORY[i].dirty == true) {
            // clear out existing memory with new prop
            result = new (&PROP_MEMORY[i]) coin_prop (p);
            return result;
        }
    }
    
    return result;
}

/* =============================== */
coin_prop::coin_prop (const point &p)
{
    static coin_handler *_handler = new (std::nothrow) coin_handler;
    handler = _handler;
    dirty   = false;
    pos     = p;
    c       = { 1.0f, 0.0f, 0.0f };
}

/* =============================== */
void coin_handler::handle (avatar *a)
{
    a->pickup(ITEM_COIN);
}

/* === POTION ITEM === */

/* ================================= */
prop* potion_prop::create (const point &p)
{
    prop *result = 0;
    for (int i = 0; i < MAX_PROPS; i++) {
        if (PROP_MEMORY[i].dirty == true) {
            // clear out existing memory with new prop
            result = new (&PROP_MEMORY[i]) potion_prop (p);
            return result;
        }
    }
    
    return result;
}

/* =================================== */
potion_prop::potion_prop (const point &p)
{
    static potion_handler *_handler = new (std::nothrow) potion_handler;
    handler = _handler;
    dirty   = false;
    pos     = p;
    c       = { 0.0f, 1.0f, 0.0f };
}

/* =============================== */
void potion_handler::handle (avatar *a)
{
    a->pickup(ITEM_POTION);
}

/* === FOOD ITEM === */

/* ================================= */
prop* food_prop::create (const point &p)
{
    prop *result = 0;
    for (int i = 0; i < MAX_PROPS; i++) {
        if (PROP_MEMORY[i].dirty == true) {
            // clear out existing memory with new prop
            result = new (&PROP_MEMORY[i]) food_prop (p);
            return result;
        }
    }
    
    return result;
}

/* =================================== */
food_prop::food_prop (const point &p)
{
    static food_handler *_handler = new (std::nothrow) food_handler;
    handler = _handler;
    dirty   = false;
    pos     = p;
    c       = { 0.0f, 0.0f, 1.0f };
}

/* =============================== */
void food_handler::handle (avatar *a)
{
    a->pickup(ITEM_FOOD);
}





