#include "controller.h"
#include "mancubus.h"
#include "avatar.h"
#include "ai_helpers.h"

using namespace kami;

animation *mancubus::ANIM_STAND = new (std::nothrow) anim_loop    (0,  2, 0.05f);
animation *mancubus::ANIM_WALK  = new (std::nothrow) anim_loop    (2,  7, 0.05f);
animation *mancubus::ANIM_DIE   = new (std::nothrow) anim_oneshot (7,  5, 0.025f);

/* ========================================== */
mancubus::mancubus(md2model *model) :
npc       (model),
m_brain   (new (std::nothrow) statemachine<mancubus>(this)),
m_handler (new (std::nothrow) mancubus_handler(this)),
m_timer   (0)
{
    m_model          = model;
    kami::vector adj = { 0.0, 0.0, 0.0 };
    m_model->adjust  = adj;
    
    set_animation(ANIM_STAND);
    
    physics_thing()->handler = m_handler;
    
    m_brain->set_current(mancubus_state::wait::instance());
    
    npc_vitals.hitpoints = 200;
    npc_vitals.state     = vitals::ALIVE;
}

/* =============== */
mancubus::~mancubus()
{

}

/* =============== */
void mancubus::step()
{
    m_brain->update();
    m_current->step(m_animstate);
}

/* ================ */
void mancubus::stand()
{
    set_animation(ANIM_STAND);
}

/* =============================== */
void mancubus::move(const short &dir)
{
    set_animation(ANIM_WALK);
    npc::move(dir);
}

/* ================= */
void mancubus::attack()
{

}

/* =============== */
void mancubus::kill()
{
    set_animation(ANIM_DIE);
    m_brain->change(mancubus_state::dying::instance());
}

/* ======================= */
void mancubus::render_model()
{
    m_model->render(m_animstate);
}

// --- wait state

/* ================================================ */
mancubus_state::wait *mancubus_state::wait::instance()
{
    static mancubus_state::wait *__instance = new mancubus_state::wait;
    return __instance;
}

/* =========================================== */
void mancubus_state::wait::enter(mancubus *actor)
{
    actor->m_timer = 0;
    actor->stand();
}

/* ============================================= */
void mancubus_state::wait::execute(mancubus *actor)
{
    actor->m_timer++;
    if(actor->m_timer > 100) {
        actor->m_brain->set_current(mancubus_state::chase::instance());
    }
}

/* ========================================== */
void mancubus_state::wait::exit(mancubus *actor)
{
    actor->m_timer = 0;
}

// --- wait state

/* ================================================== */
mancubus_state::chase *mancubus_state::chase::instance()
{
    static mancubus_state::chase *__instance = new mancubus_state::chase;
    return __instance;
}

/* ============================================ */
void mancubus_state::chase::enter(mancubus *actor)
{
    
}

/* ============================================== */
void mancubus_state::chase::execute(mancubus *actor)
{
    avatar *_avatar = controller::instance()->the_avatar;
    vector dir = pmath::vector_from_point(_avatar->get_position(), actor->get_position());
    float dist = vmath::len(dir);
    
    if(dist > 4.0f && dist < 8.0f) {
        vector dir     = pmath::vector_from_point(_avatar->get_position(), actor->get_position());
        uint8_t io_dir = ai_helpers::get_move_dir(dir);
        
        actor->move(io_dir);
    } else {
        actor->m_brain->set_current(mancubus_state::wait::instance());
    }
}

/* =========================================== */
void mancubus_state::chase::exit(mancubus *actor)
{
    
}

// --- dying state

/* ================================================== */
mancubus_state::dying *mancubus_state::dying::instance()
{
    dying *__instance = new dying;
    return __instance;
}

/* ============================================= */
void mancubus_state::dying::enter (mancubus *actor)
{
    
}

/* =============================================== */
void mancubus_state::dying::execute (mancubus *actor)
{
    if(actor->m_animstate.fin) {
        actor->npc_vitals.state = vitals::DEAD;
    }
}

/* ============================================ */
void mancubus_state::dying::exit (mancubus *actor)
{
    
}





