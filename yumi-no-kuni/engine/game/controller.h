//
//  controller.h
//  kami
//
//  Created by John Evans Wagenaar on 2012/03/21.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef kami_controller_h
#define kami_controller_h

#include "kami.h"
#include "hidsys.h"
#include "scene.h"
#include "avatar.h"
#include "world.h"
#include "sector.h"
#include "sprite.h"
#include "ingame_hud.h"
#include "io.h"
#include "menu.h"

#include <thread>

#define KAMI_TIME_NOW        std::chrono::high_resolution_clock::now()
#define KAMI_TIME_DELTA(S,E) std::chrono::duration_cast<std::chrono::seconds>(E - S).count()
#define KAMI_TIME_FINE(S,E)  std::chrono::duration_cast<std::chrono::milliseconds>(E - S).count()

typedef std::chrono::time_point<std::chrono::high_resolution_clock> time_point;

namespace kami
{
    class controller
    {
        world    *m_world = new (std::nothrow) world;
        scene    *m_scene = new (std::nothrow) scene;
        mainmenu *m_menu  = 0;
        
        float  m_time  = 0.1f;
        
        uint16_t m_pausetimeout = 0;
    public:
        static const uint16_t INIT = 0x00;
        static const uint16_t MENU = 0x01;
        static const uint16_t GAME = 0x02;
        static const uint16_t QUIT = 0x03;
        
        uint16_t m_state = INIT;
        
        ingame_hud *the_hud = 0;
        hidsystem  *the_hid = new (std::nothrow) hidsystem;
        
        avatar    *the_avatar;
        sector    *the_sector;
        
        bool ready = false;
        
        static controller *instance (void);
        
        void step     (void);
        void render   (void);
        void shutdown (void);
        void init     (void);
        
    protected:
        void user_input   (void);
        void world_update (void);
        void process_menu (void);
    private:
         controller (void);
        ~controller (void);

        void free_resources (void);
    };
}





#endif
