#ifndef kami_gamemeta_h
#define kami_gamemeta_h

#include "primitives.h"

namespace kami {
    /*
     * == Meta data for a npc ==
     */
    typedef struct t_npc_meta {
        uint16_t id;
        
        metamodel mm;
        point    pos;
    } npc_meta;
    
    
    /*
     * == Meta data for a sector ==
     */
    typedef struct t_sector_meta {
        uint16_t id;
        
        metamodel render_mm;
        metamodel bbox_mm;
        metamodel skydome_mm;
    } sector_meta;
    
    /*
     *  == Meta data for an avatar ==
     */
    typedef struct t_avatar_meta {
        uint16_t id;
        
        point     pos;
        metamodel mm;
    } avatar_meta;
    
    /**
     * == Meta data for an item
     */
    typedef struct t_item_meta {
        uint16_t id;
        
        point     pos;
        metamodel mm;
    } item_meta;
}

#endif
