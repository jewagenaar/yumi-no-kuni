#ifndef __yumi_no_kuni__ingame_hud__
#define __yumi_no_kuni__ingame_hud__

#include "hudlayer.h"
#include "item.h"

#include <string>

namespace kami
{
    class ingame_hud : public hudlayer
    {
        sprite   *m_statushud;
        sprite   *m_inventory;
    
        textout *m_potioncounter;
        textout *m_foodcounter;
        textout *m_crytalcounter;
        
        std::string m_potionout;
        std::string m_foodout;
        std::string m_crystalout;
        
    public:
        
         ingame_hud ();
        ~ingame_hud (void);
        
        void step (void);
        
        void render_text();
    };
}

#endif /* defined(__yumi_no_kuni__ingame_hud__) */
