#include <GLUT/GLUT.h>
#include "effects.h"

using namespace kami;

explosion  *effect::EXPLOSION  = new (std::nothrow) explosion;
splat      *effect::SPLAT      = new (std::nothrow) splat;
boom_splat *effect::BOOM_SPLAT = new (std::nothrow) boom_splat;

effect_state* effect::create(const point &pos, effect *eff)
{
    static effect_state _effcache [100];
    static effect_state *null_state = new effect_state;
    
    for (int i = 0; i < 100; i++) {
        if (_effcache[i].fin) {
            effect_state *result = new (&_effcache[i]) effect_state;
            
            result->fin = false;
            result->pos = pos;
            result->eff = eff;
            result->cur = 0;
            
            return result;
        }
    }
    
    return null_state; // could not allocate
}

void explosion::step (effect_state *es)
{
    if (es->cur < m_fc) {
        es->cur++;
    } else {
        es->fin = true;
    }
}

void explosion::render (effect_state *es)
{
    glUseProgram(0);
    glColor3f(0.5f, 0.0f, 0.0f);
    glPushMatrix();
    glTranslatef(es->pos.x, es->pos.y, es->pos.z);
    glutSolidSphere(es->cur * 0.1f, 8, 8);
    glPopMatrix();
}

void splat::step (effect_state *es)
{
    if (es->cur < m_fc) {
        es->cur++;
    } else {
        es->fin = true;
    }
}

void splat::render (effect_state *es)
{
    glUseProgram(0);
    glEnable(GL_BLEND);
    glBlendEquation(GL_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4f(0.5f, 0.0f, 0.0f, 1.0f - (float) es->cur / 255.0f);
    glPushMatrix();
    glTranslatef(es->pos.x, es->pos.y, 0.0001f);
    glBegin(GL_POLYGON);
    glVertex2f(-1.0f,  1.0f);
    glVertex2f(-1.0f, -1.0f);
    glVertex2f( 1.0f, -1.0f);
    glVertex2f( 1.0f,  1.0f);
    glEnd();
    glDisable(GL_BLEND);
    glPopMatrix();
}

void boom_splat::step (effect_state *es)
{
    if (es->cur < EXPLOSION->m_fc) {
        es->cur++;
        if (es->cur == EXPLOSION->m_fc) {
            es->eff = SPLAT;
        }
    } else {
        es->cur++;
        if (es->cur == SPLAT->m_fc) {
            es->fin = true;
        }
    }
}

void boom_splat::render (effect_state *es)
{
    if (es->cur < EXPLOSION->m_fc) {
        EXPLOSION->render(es);
    } else {
        SPLAT->render(es);
    }
}
