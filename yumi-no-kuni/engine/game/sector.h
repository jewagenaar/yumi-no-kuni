//
//  sector.h
//  kami
//
//  Created by John Evans Wagenaar on 2012/03/18.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef kami_sector_h
#define kami_sector_h

#include <vector>

#include "avatar.h"
#include "npc.h"
#include "skydome.h"
#include "renderable.h"
#include "colshapes.h"
#include "projectile.h"
#include "effects.h"
#include "item.h"

namespace kami
{
    class sector : public renderable
    {
        wfmodel *m_model     = 0;
        float    m_animtimer = 0;
    public:
        std::vector<effect_state *> effects;
        std::vector<projectile *>   projectiles;
        std::vector<prop *>         props;
        std::vector<npc *>          npcs;

        sector *north = 0;
        sector *south = 0;
        sector *east  = 0;
        sector *west  = 0;
        
        point  *points;
        normal *up_dir;
        
        bbox   *col_data;
        
        sector  (void);
        ~sector (void);
        
        // set the modeldata for this sector
        void set_renderable (wfmodel *model);
        
        // update the members of the sector
        void step (void);
        
        // render the sector
        void render (void);
        
    protected:
        void render (point *placement);
        
        void killoff (npc *n);
    };
}




#endif
