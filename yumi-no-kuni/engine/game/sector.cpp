//
//  sector.cpp
//  kami
//
//  Created by John Evans Wagenaar on 2012/03/18.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include <cstdlib>
#include "kami.h"
#include "sector.h"
#include "kamimath.h"
#include "controller.h"
#include "item.h"

kami::point *placement_center = new kami::point({   0.0f,    0.0f, 0.0f});
kami::point *placement_north  = new kami::point({   0.0f,  TILE_H, 0.0f});
kami::point *placement_south  = new kami::point({   0.0f, -TILE_H, 0.0f});
kami::point *placement_east   = new kami::point({ TILE_W,    0.0f, 0.0f});
kami::point *placement_west   = new kami::point({-TILE_W,    0.0f, 0.0f});

using namespace kami;

sector::sector() 
{
}

/* =========== */
sector::~sector()
{
}

/* ===================================== */
void sector::set_renderable(wfmodel *model)
{
    m_model = model;
}

/* ============= */
void sector::step()
{
    // update all the NPCs
    for (auto iter = npcs.begin(); iter < npcs.end(); iter++) {
        npc *n = *iter;
        n->step();

        if (n->npc_vitals.state == vitals::DEAD) {
            // killoff the NPC
            killoff(n);
            // dead NPC - remove from list
            npcs.erase(iter);
            
        } else if (n->npc_vitals.hitpoints == 0 &&
                   n->npc_vitals.state     == vitals::ALIVE) {
            
            // alive with no health - kill it
            n->kill();
        }
    }
    
    // update all the projectiles
    for (auto iter = projectiles.begin(); iter < projectiles.end(); iter++) {
        projectile *p = *iter;
        p->step();
        
        if (p->hit) {
            projectiles.erase(iter);
            effect_state *eff = effect::create(p->pos, effect::EXPLOSION);
            effects.push_back(eff);
        }
    }
    
    // update all the props
    for (auto iter = props.begin(); iter < props.end(); iter++) {
        prop *p = *iter;
        
        if (p->hit) {
            p->handler->handle (controller::instance()->the_avatar);
            p->dirty = true;
            props.erase(iter);
        }
    }
    
    // update the effects
    for (auto iter = effects.begin(); iter < effects.end(); iter++) {
        effect_state *eff = *iter;
        eff->step();
        if (eff->fin) {
            effects.erase(iter);
        }
    }
}

/* =============== */
void sector::render()
{
    render(::placement_center);
    
    controller::instance()->the_avatar->render();
    
    if(north) {
        north->render(::placement_north);
    }
    
    if(south) {
        south->render(::placement_south);
    }
    
    if(east) {
        east->render(::placement_east);
    }
    
    if(west) {
        west->render(::placement_west);
    }
    
}

/* =============================== */
void sector::render(point *placement)
{
#ifdef __KAMI_DEBUG__
    assert(m_model);
#endif
    
    glPushMatrix();
    
    glTranslatef(placement->x, placement->y, placement->z);
    
    // render the sector's model
    m_model->render();
    
    // now render all stationary objects eg. items, weapons, decorations etc.
    for (auto iter = props.begin(); iter < props.end(); iter++) {
        prop *p = *iter;
        p->render();
    }
    
    // render the NPCs
    for (auto iter = npcs.begin(); iter < npcs.end(); iter++) {
        npc *n = *iter;
        n->render();
    }
    
    // render all the projectiles
    for (auto iter = projectiles.begin(); iter < projectiles.end(); iter++) {
        projectile *proj = *iter;
        proj->render();
    }
    
    // render all the effects
    for (auto iter = effects.begin(); iter < effects.end(); iter++) {
        effect_state *eff = *iter;
        eff->render();
    }
    
    glPopMatrix();
}

/* ======================= */
void sector::killoff (npc *n)
{
    if (n->inventory[ITEM_COIN]) {
        
        point pos = n->get_position();
        pos.x += (float) (rand() % 100) / 100.0f;
        pos.y += (float) (rand() % 100) / 100.0f;
        
        prop *p = coin_prop::create(pos);
        if (p) {
            props.push_back(p);
        }
    }
    
    if (n->inventory[ITEM_POTION]) {
        
        point pos = n->get_position();
        pos.x += (float) (rand() % 100) / 100.0f;
        pos.y += (float) (rand() % 100) / 100.0f;
        
        prop *p = potion_prop::create(pos);
        if (p) {
            props.push_back(p);
        }
    }
    
    if (n->inventory[ITEM_FOOD]) {
        
        point pos = n->get_position();
        pos.x += (float) (rand() % 100) / 100.0f;
        pos.y += (float) (rand() % 100) / 100.0f;
        
        prop *p = food_prop::create(pos);
        if (p) {
            props.push_back(p);
        }
    }
    
    // make a splat where the NPC died
    effect_state *eff = effect::create(n->get_position(), effect::BOOM_SPLAT);
    effects.push_back(eff);
}


