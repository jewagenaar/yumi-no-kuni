#include "ingame_hud.h"
#include "global_ids.h"
#include "texman.h"
#include "controller.h"
#include "item.h"

#include <sstream>

using namespace kami;

ingame_hud::ingame_hud ()
{
    point debug_pos = { 0.4f, -0.9f, -1.0f };
    
    debugout            = new textout;
    debugout->position  = debug_pos;
    debugout->fontstyle = font::debug();
    
    m_statushud = new sprite ();
    m_statushud->texid = texman::instance()->get_texture(IMG_STATUSHUD);
    
    m_statushud->width  = 0.75f;
    m_statushud->height = 0.25f;

    m_statushud->pos.x = -1.0f;
    m_statushud->pos.y =  1.0f;
    m_statushud->pos.z = -1.0f;
    
    m_inventory = new sprite ();
    m_inventory->texid  = texman::instance()->get_texture(IMG_INVENTORY);
    m_inventory->width  = 0.47f;
    m_inventory->height = 0.2f;
    
    m_inventory->pos.x =  0.5f;
    m_inventory->pos.y =  1.0f;
    m_inventory->pos.z = -1.0f;
    
    point crystal_pos = { 0.52f, 0.75f, -1.1f };
    m_crytalcounter = new textout;
    m_crytalcounter->position = crystal_pos;
    m_crytalcounter->fontstyle = font::debug();
    
    point potion_pos = { 0.66f, 0.75f, -1.1f };
    m_potioncounter = new textout;
    m_potioncounter->position = potion_pos;
    m_potioncounter->fontstyle = font::debug();
    
    point food_pos = { 0.80f, 0.75f, -1.1f };
    m_foodcounter = new textout;
    m_foodcounter->position = food_pos;
    m_foodcounter->fontstyle = font::debug();
}

/* ======================== */
ingame_hud::~ingame_hud (void)
{
    delete m_statushud;
    delete m_inventory;
    
    delete m_potioncounter;
    delete m_foodcounter;
    delete m_crytalcounter;
    
    delete debugout;
}

/* ================= */
void ingame_hud::step()
{
    hudlayer::clear ();
    
    avatar *_avatar = controller::instance()->the_avatar;
    
    
    if (_avatar->inventory[ITEM_COIN]) {
        hudlayer::add(coin_item::get_sprite());
        std::stringstream str;
        str << _avatar->inventory[ITEM_COIN];
        m_crystalout = str.str();
    } else {
        m_crystalout = "0";
    }
    
    if (_avatar->inventory[ITEM_POTION]) {
        hudlayer::add(potion_item::get_sprite());
        std::stringstream str;
        str << _avatar->inventory[ITEM_POTION];
        m_potionout = str.str();
    } else {
        m_potionout = "0";
    }
    
    if (_avatar->inventory[ITEM_FOOD]) {
        hudlayer::add(food_item::get_sprite());
        std::stringstream str;
        str << _avatar->inventory[ITEM_FOOD];
        m_foodout = str.str();
    } else {
        m_foodout = "0";
    }
    
    hudlayer::add (m_statushud);
    hudlayer::add (m_inventory);
}

/* ======================== */
void ingame_hud::render_text()
{
    m_crytalcounter->render (m_crystalout.c_str());
    m_potioncounter->render (m_potionout.c_str());
    m_foodcounter  ->render (m_foodout.c_str());
    
    hudlayer::render_text ();
}
