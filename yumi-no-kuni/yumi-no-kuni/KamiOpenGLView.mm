#import "KamiOpenGLView.hpp"
#import <OpenGL/gl.h>
#import <OpenGL/glu.h>

@implementation kamiOpenGLView

- (void) prepareOpenGL
{
    NSOpenGLContext *context = [self openGLContext];
    [context makeCurrentContext];
        
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0, 1.0f, 1.0f, 100.0f);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glEnable(GL_DEPTH_TEST);
    glViewport(0, 0, 800, 800);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
}

- (void) reshape
{
    NSRect rect = [self convertRectToBase:[self bounds]];
    glViewport(0, 0, rect.size.width, rect.size.height);
}

- (void) drawRect:(NSRect)dirtyRect
{
    if (kami::controller::instance()->ready) {
        kami::controller::instance()->render();
    }
    [[self openGLContext] flushBuffer];
}

- (void) keyDown:(NSEvent *)theEvent
{
    unsigned short keyCode = [theEvent keyCode];
    kami::controller::instance()->the_hid->key_down(keyCode);
}

- (void) keyUp:(NSEvent *)theEvent
{
    unsigned short keyCode = [theEvent keyCode];
    kami::controller::instance()->the_hid->key_up(keyCode);
}

- (BOOL) acceptsFirstResponder
{
    return YES;
}

- (BOOL) becomeFirstResponder
{
    return YES;
}

- (BOOL) resignFirstResponder
{
    return YES;
}

- (BOOL) isOpaque
{
    return YES;
}

@end
