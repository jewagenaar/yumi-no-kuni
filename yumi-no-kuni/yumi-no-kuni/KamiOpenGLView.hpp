//
//  kamiOpenGLView.h
//  kami
//
//  Created by John Evans Wagenaar on 2012/05/13.
//  Copyright (c) 2012 jewagenaar@gmail.com. All rights reserved.
//

#import <AppKit/AppKit.h>
#import "kami.h"
#import "controller.h"
#import "shaderman.h"
#import "loader.h"

@interface kamiOpenGLView : NSOpenGLView

@end
