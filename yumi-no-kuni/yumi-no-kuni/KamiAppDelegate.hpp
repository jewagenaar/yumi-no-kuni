//
//  kamiAppDelegate.h
//  kami
//
//  Created by John Evans Wagenaar on 2012/05/13.
//  Copyright (c) 2012 jewagenaar@gmail.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "KamiOpenGLView.hpp"

@interface kamiAppDelegate : NSObject <NSApplicationDelegate>
{
    NSTimer  *_timer;
}
@property (assign) IBOutlet kamiOpenGLView *kamiOpenGLView;

@property (assign) IBOutlet NSWindow *window;

@end
