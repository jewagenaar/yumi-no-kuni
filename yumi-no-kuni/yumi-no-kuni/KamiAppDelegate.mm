#import "KamiAppDelegate.hpp"
#import "kami.h"
#import "controller.h"
#import "shaderman.h"
#import "audioenv.h"

@implementation kamiAppDelegate

@synthesize kamiOpenGLView = _kamiOpenGLView;
@synthesize window = _window;

- (void) dealloc
{
    NSLog(@"yumi-no-kumi terminating");
    
    kami::controller::instance()->shutdown();
    kami::audioenv::stop();
    
    [super dealloc];
}

- (void) applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // start the audio environment
    kami::audioenv::start();
    
    // get the resources path
    NSBundle *_bundle = [NSBundle mainBundle];
    const char* baseDir = [[_bundle resourcePath] UTF8String];
    kami::loader::instance()->set_resource_path(baseDir);
    
    // start the controller and pass in the demo level
    kami::controller::instance()->the_sector = kami::loader::instance()->demo_level();
    kami::controller::instance()->the_avatar = kami::loader::instance()->demo_avatar();
    kami::controller::instance()->init();
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:1.0f/100.0f target:self
                                            selector:@selector(stepGame) userInfo:nil repeats:YES];
    [_timer retain];
    
    [[NSRunLoop currentRunLoop] addTimer:_timer forMode: NSDefaultRunLoopMode];
    [[NSRunLoop currentRunLoop] addTimer:_timer forMode: NSEventTrackingRunLoopMode];
}

- (void) stepGame
{
    if(![[NSApplication sharedApplication] isHidden]) {
        
        kami::controller::instance()->step();
        
        if (kami::controller::instance()->m_state == kami::controller::QUIT) {
            [[NSApplication sharedApplication] terminate:self];
        } else {
            [_kamiOpenGLView setNeedsDisplay:YES];
        }
    }
}

@end
