//
//  main.m
//  kami
//
//  Created by John Evans Wagenaar on 2012/05/13.
//  Copyright (c) 2012 jewagenaar@gmail.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
